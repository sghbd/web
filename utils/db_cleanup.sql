select table_name from user_tables;
select sequence_name from user_sequences;

drop table AUTH_PERMISSION;
drop table AUTH_MEMBERSHIP;
drop table AUTH_CAS;
drop table AUTH_EVENT;
drop table AUTH_GROUP;
drop table AUTH_USER;

drop sequence "DBSIMUL"."APPEP_AUTH_GROUP_sequence";
drop sequence "DBSIMUL"."APPEP_AUTH_MEMBERSHIP_sequence";
drop sequence "DBSIMUL"."APPEP_AUTH_USER_sequence";
drop sequence "DBSIMUL"."APP_AUTH_CAS_sequence";
drop sequence "DBSIMUL"."APP_AUTH_EVENT_sequence";
drop sequence "DBSIMUL"."APP_AUTH_GROUP_sequence";
drop sequence "DBSIMUL"."APP_AUTH_MEMBERSHIP_sequence";


-- CREATE TABLE app_auth_user(
--     id NUMBER PRIMARY KEY,
--     first_name VARCHAR2(128),
--     last_name VARCHAR2(128),
--     email VARCHAR2(512),
--     password VARCHAR2(512),
--     registration_key VARCHAR2(512),
--     reset_password_key VARCHAR2(512),
--     registration_id VARCHAR2(512)
-- );
