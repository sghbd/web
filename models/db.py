# -*- coding: utf-8 -*-
import os
from pydal.adapters.sqlite import SQLiteAdapter
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth, Service, PluginManager
from gluon.custom_import import track_changes
from gluon import current

VERSION = '0.09.20'

if request.is_local:
    track_changes(True)

if request.global_settings.web2py_version < "2.14.1":
    raise HTTP(500, "Requires web2py 2.13.3 or newer")

myconf = AppConfig(reload=True)

os.environ['NLS_LANG'] = 'American_America.UTF8'
# os.environ['ORACLE_SID'] = 'EPARAGON_TEST'
# os.environ['ORACLE_HOME'] = '/usr/lib/oracle/12.2/client64'
# os.environ['LD_LIBRARY_PATH'] = '/usr/lib/oracle/12.2/client64/lib;$LD_LIBRARY_PATH'
db = DAL(myconf.get('db.uri'),
         pool_size=myconf.get('db.pool_size'),
         migrate_enabled=myconf.get('db.migrate'),
         lazy_tables=False,
        #  fake_migrate=True,
        #  fake_migrate_all=True,
         entity_quoting=False,
         check_reserved=['all'])
is_local_db = type(db._adapter) == SQLiteAdapter

response.generic_patterns = ['*']  # if request.is_local else []
response.formstyle = myconf.get('forms.formstyle')  # or 'bootstrap3_stacked' or 'bootstrap2' or other
response.form_label_separator = myconf.get('forms.separator') or ''


auth = Auth(db, host_names=myconf.get('host.names'))
service = Service()
plugins = PluginManager()

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
# mail.settings.server = 'logging' if request.is_local else myconf.get('smtp.server')
mail.settings.server = myconf.get('smtp.server')
mail.settings.sender = myconf.get('smtp.sender')
mail.settings.login = myconf.get('smtp.login')
mail.settings.tls = myconf.get('smtp.tls') or False
mail.settings.ssl = myconf.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.login_after_registration = True
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# auth.settings.table_user_name = 'app_auth_user'
# auth.settings.table_group_name = 'app_auth_group'
# auth.settings.table_membership_name = 'app_auth_membership'
# auth.settings.table_permission_name = 'app_auth_permission'
# auth.settings.table_event_name = 'app_auth_event'
# auth.settings.table_cas_name = 'app_auth_cas'

auth.settings.extra_fields['auth_user'] = [
    Field('client_ref', 'integer',
          writable=True, readable=True, default=101,
          label=T('Client ref ID')),
    # Field('pred_suffix', 'string',
    #       writable=True, readable=True, default='m1',
    #       label='pred_suffix'),
    Field('days_span', 'integer',
          writable=True, readable=True, default=365,
          label=T('Span window [days]')),
    Field('cache_timeout', 'integer',
          writable=True, readable=True, default=10,
          label=T('Cache expiration time [s]'))]

auth.define_tables(username=False, signature=False)

current.db = db
current.cache = cache
