db.define_table(
    'tm_fuel',
    Field('id', 'integer', rname='lp'),
    Field('f_date', 'date', rname='data_pomiaru'),
    Field('f_name', 'string', rname='nazwa'),
    Field('f_price', 'float', rname='cena_jedn'),
    Field('f_unit', 'string', rname='jedn'),
    primarykey=['id'],
    rname='d_paliwo',
    format='%(f_name)s [%(f_price).2fPLN/%(f_unit)s]',
    migrate=is_local_db)

db.define_table(
    'tm_tesco',
    Field('id', 'integer', rname='lp'),
    Field('f_cat', 'string', rname='kat'),
    Field('f_subcat1', 'string', rname='podkat_1'),
    Field('f_subcat2', 'string', rname='podkat_2'),
    Field('f_name', 'string', rname='nazwa'),
    Field('f_price', 'float', rname='cena_jedn'),
    Field('f_unit', 'string', rname='jedn'),
    *(
        [Field('f_p%d' % x, 'float', rname='P_%d' % x) for x in range(1, 13)] +
        [Field('f_c%d' % x, 'float', rname='C_%d' % x) for x in range(1, 13)]),
    primarykey=['id'],
    rname='d_produkty_tesco',
    format='%(f_name)s [%(f_price).2fPLN/%(f_unit)s]',
    migrate=is_local_db)

db.define_table(
    'tm_hm',
    Field('id', 'integer', rname='lp'),
    Field('f_cat', 'string', rname='kat'),
    Field('f_subcat1', 'string', rname='podkat_1'),
    Field('f_name', 'string', rname='nazwa'),
    Field('f_price', 'float', rname='cena_jedn'),
    primarykey=['id'],
    rname='d_produkty_h_and_m',
    migrate=is_local_db)

db.define_table(
    't_transactions',
    Field('f_created', 'date', rname='RE_DATASTWORZENIAEPARAGONU'),
    Field('f_issued', 'date', rname='RE_DATAWYSTAWIENIAPARAGONU'),
    Field('f_nip', 'string', rname='RE_PODATNIK_NIP'),
    Field('f_name', 'string', rname='RE_PODATNIK_NAZWA'),
    Field('f_regon', 'string', rname='RE_PODATNIK_REGON'),
    Field('f_ccode', 'string', rname='RE_ADRESPODMIOTU_KODKRAJU'),
    Field('f_district', 'string', rname='RE_ADRESPODMIOTU_WOJEWODZTWO'),
    Field('f_county', 'string', rname='RE_ADRESPODMIOTU_POWIAT'),
    Field('f_gmina', 'string', rname='RE_ADRESPODMIOTU_GMINA'),
    Field('f_street', 'string', rname='RE_ADRESPODMIOTU_ULICA'),
    Field('f_housenb', 'string', rname='RE_ADRESPODMIOTU_NRDOMU'),
    Field('f_flatnb', 'string', rname='RE_ADRESPODMIOTU_NRLOKALU'),
    Field('f_city', 'string', rname='RE_ADRESPODMIOTU_MIEJSCOWOSC'),
    Field('f_postalc', 'string', rname='RE_ADRESPODMIOTU_KODPOCZTOWY'),
    Field('f_post', 'string', rname='RE_ADRESPODMIOTU_POCZTA'),
    Field('f_sell_ccode', 'string', rname='RE_PUNKTSPRZEDAZY_KODKRAJU'),
    Field('f_sell_district', 'string', rname='RE_PUNKTSPRZEDAZY_WOJEWODZTWO'),
    Field('f_sell_county', 'string', rname='RE_PUNKTSPRZEDAZY_POWIAT'),
    Field('f_sell_gmina', 'string', rname='RE_PUNKTSPRZEDAZY_GMINA'),
    Field('f_sell_street', 'string', rname='RE_PUNKTSPRZEDAZY_ULICA'),
    Field('f_sell_housenb', 'string', rname='RE_PUNKTSPRZEDAZY_NRDOMU'),
    Field('f_sell_flatnb', 'string', rname='RE_PUNKTSPRZEDAZY_NRLOKALU'),
    Field('f_sell_city', 'string', rname='RE_PUNKTSPRZEDAZY_MIEJSCOWOSC'),
    Field('f_sell_postalc', 'string', rname='RE_PUNKTSPRZEDAZY_KODPOCZTOWY'),
    Field('f_sell_post', 'string', rname='RE_PUNKTSPRZEDAZY_POCZTA'),
    Field('f_name', 'string', rname='PR_NAZWA'),
    Field('f_unit', 'string', rname='PR_JEDNOSTKA'),
    Field('f_unitnb', 'float', rname='PR_ILOSCJEDNOSTEK'),
    Field('f_unitprice', 'float', rname='PR_CENAZAJEDNAJEDNOSTKA'),
    Field('f_price', 'float', rname='PR_CENAZAZAKUPIONAJEDNOSTKE'),
    Field('f_cat', 'string', rname='PR_KATEGORIAPRODUKTU'),
    Field('f_taxcat', 'string', rname='PR_KATEGORIAPODATKOWA'),
    Field('f_timestamp', 'datetime', rname='DB_INSERTTIMESTAMP'),
    Field('client_id', 'bigint', rname='DB_KLIENTREFERENCJA'),
    primarykey=['client_id'],
    rname='transaction_products',
    migrate=is_local_db)

db.define_table(
    't_transactions_week',
    Field('client_ref', 'integer', rname='ID_KLIENTA'),
    Field('f_date', 'date', rname='DATA_TYG'),
    Field('f_expenses', 'float', rname='WYDATKI_TYG'),
    primarykey=['f_date'],
    rname='transactions_agregaty',
    migrate=is_local_db)

db.define_table(
    't_transactions_week_cat',
    Field('client_ref', 'integer', rname='ID_KLIENTA'),
    Field('f_date', 'date', rname='DATA_TYG'),
    Field('f_expenses', 'float', rname='WYDATKI_TYG'),
    Field('f_cat', 'string', rname='KATEGORIA'),
    primarykey=['f_date'],
    rname='transactions_agregaty_kat',
    migrate=is_local_db)

db.define_table(
    't_predictions_week',
    Field('client_ref', 'integer', rname='ID_KLIENTA'),
    Field('f_date', 'date', rname='DATA_TYG'),
    Field('f_expenses', 'float', rname='WYDATKI_TYG'),
    primarykey=['f_date'],
    rname='transactions_prognozy',
    migrate=is_local_db)

# problem z kolumną date
db.define_table(
    't_predictions_week_cat',
    Field('client_ref', 'integer', rname='ID_KLIENTA'),
    Field('f_date', 'date', rname='DATA_TYG'),
    Field('f_expenses', 'float', rname='WYDATKI_TYG'),
    Field('f_cat', 'string', rname='KATEGORIA'),
    primarykey=['f_date'],
    rname='transactions_prognozy_kat',
    migrate=is_local_db)


for i in range(1, 4):
    db.define_table(
        't_paragon_m%d' % i,
        Field('f_date', 'date', rname='data_zakupu'),
        Field('f_name', 'string', rname='nazwa'),
        Field('f_cat', 'string', rname='kategoria'),
        Field('f_price', 'float', rname='cena'),
        primarykey=['f_date'],
        rname='p_paragon_m%d' % i,
        migrate=is_local_db)
