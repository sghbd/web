# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# ----------------------------------------------------------------------------------------------------------------------
# Customize your APP title, subtitle and menus here
# ----------------------------------------------------------------------------------------------------------------------

response.logo = A(B('eparagon'),
                  _class="navbar-brand", _href="/eparagon",
                  _id="web2py-logo")
response.title = request.application.replace('_', ' ').title()
response.subtitle = ''

# ----------------------------------------------------------------------------------------------------------------------
# read more at http://dev.w3.org/html5/markup/meta.name.html
# ----------------------------------------------------------------------------------------------------------------------
response.meta.author = myconf.get('app.author')
response.meta.description = myconf.get('app.description')
response.meta.keywords = myconf.get('app.keywords')
response.meta.generator = myconf.get('app.generator')

# ----------------------------------------------------------------------------------------------------------------------
# your http://google.com/analytics id
# ----------------------------------------------------------------------------------------------------------------------
response.google_analytics_id = None

# ----------------------------------------------------------------------------------------------------------------------
# this is the main application menu add/remove items as required
# ----------------------------------------------------------------------------------------------------------------------

cat_submenu = db(
    db.t_transactions_week_cat.client_ref == 101).select(
        db.t_transactions_week_cat.f_cat,
        distinct=True,
        cache=(cache.ram, 300),
        cacheable=True)

response.menu = [
    (T('My stats'), False, URL('default', 'index'),
        [(T('General'), False, URL('default', 'index'), [])] +
        [(T('General detailed'), False,
          URL('default', 'index', args='stack'), [])] +
        [(r.f_cat.title(), False,
          URL('default', 'category',
              args=r.f_cat.replace(' ', '_').replace(',', '__')),
         []) for r in cat_submenu]),
    (T('Global stats'), False, URL('default', 'gstats'), []),
    (T('Cash register'), False, URL('cash', 'index'), []),
    (T('Last generated paragon'), False,
     A(T('Last generated paragon'),
       _href=URL('cash', 'lastep'), _target='new'), [])
    ]

if "auth" in locals():
    auth.wikimenu()
