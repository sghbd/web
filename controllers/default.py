# -*- coding: utf-8 -*-
from datetime import timedelta
import pandas as pd
import numpy as np
from bokeh.plotting import figure
from bokeh.charts import Bar, Line, HeatMap, bins
from bokeh.layouts import column, row
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh.models import CheckboxGroup, CustomJS

SWIDTH = 1920
SHEIGHT = 600


def __last_entry(client_ref):
    try:
        return db(
            db.t_transactions_week.client_ref == client_ref).select(
                orderby=~db.t_transactions_week.f_date,
                limitby=(0, 1),
                cache=(cache.disk, auth.user.cache_timeout),
                cacheable=True).first()['f_date']
    except TypeError:
        return None


@auth.requires_login()
# @cache.action(time_expire=300, cache_model=cache.disk, quick='SVLU')
def index():
    args = request.args
    WITH_CATS = False
    if len(args) > 0 and args[0] == 'stack':
        WITH_CATS = True

    CLIENT_REF = auth.user.client_ref
    DAYS_SPAN = auth.user.days_span

    SWIDTH = 1920
    SHEIGHT = 1200 if WITH_CATS else 600

    config = dict(
        t_transactions_week_cat=dict(
            color='#3ca4de', label=T('My expenses / week')),
        t_predictions_week_cat=dict(
            color='#db6e29', label=T('My predicted future expenses / week')))

    plots = []

    last_entry = __last_entry(CLIENT_REF)
    if not last_entry:
        return dict(
            title=T('My stats'),
            content=T('Could not find last entry :('))

    for table_name in config:
        key = table_name
        if not WITH_CATS:
            table_name = table_name[:-4]
        if 't_transactions_week' in table_name:
            query = db[table_name].f_date > (
                last_entry - timedelta(days=DAYS_SPAN))
        else:
            query = (db[table_name].f_date > last_entry) & \
                (db[table_name].f_date < last_entry + timedelta(
                    days=DAYS_SPAN))
        rows = db(query)(
            db[table_name].client_ref == CLIENT_REF).select(
            orderby=db[table_name].f_date,
            cache=(cache.disk, auth.user.cache_timeout),
            cacheable=True)

        if len(rows) == 0:
            return dict(
                title=T('My stats'),
                content=T('Lack of sufficient amount of '
                          'data to render the plots :('))

        legend = 'bottom_left' if WITH_CATS else None
        data = pd.DataFrame(rows.as_list())
        color = None if WITH_CATS else config[key]['color']
        stack = 'f_cat' if WITH_CATS else None
        bar = Bar(
            data, 'f_date', values='f_expenses', legend=legend,
            stack=stack, agg='sum', color=color,
            width=SWIDTH, height=SHEIGHT, sizing_mode='scale_width',
            xlabel=str(T('Week')),
            ylabel=str(T('Spent')) + ' [PLN]',
            title=str(config[key]['label']))

        if not WITH_CATS:
            plots.append(bar)
            continue

        bar.legend.location = "top_left"
        bar.legend.background_fill_alpha = .4

        checkboxes = CheckboxGroup(
            labels=np.unique(data['f_cat']).tolist(),
            active=list(range(np.unique(data['f_cat']).size)))
        checkboxes.callback = CustomJS(args=dict(bars=bar), code="""

        var group = '%s';

        function change_bars_visibility(checkbox_name, visible) {
            for (j = 0; j < bars.renderers.length; j++) {
                if (bars.renderers[j].attributes.hasOwnProperty('data_source') &&
                    bars.renderers[j].data_source.data[group][0] === checkbox_name) {
                    bars.renderers[j].visible = visible;
                }
            }
        }

        for (i = 0; i < cb_obj.labels.length; i++) {
            var checkbox_name = cb_obj.labels[i];

            if (cb_obj.active.indexOf(i) >= 0) {
                change_bars_visibility(checkbox_name, true);
            }
            else {
                change_bars_visibility(checkbox_name, false);
            }
        }
        """ % 'f_cat')

        plots.append(row(bar, checkboxes,
                     width=1280, height=SHEIGHT, sizing_mode='scale_width'))

    return dict(
        title=T('My stats'),
        content=XML(file_html(column(
            plots, width=SWIDTH, height=SHEIGHT,
            sizing_mode='scale_width'), CDN)))


@auth.requires_login()
# @cache.action(time_expire=300, cache_model=cache.disk, quick='SVLU')
def category():
    cat = request.args[0].replace('__', ',').replace('_', ' ')

    CLIENT_REF = auth.user.client_ref
    DAYS_SPAN = auth.user.days_span

    desc_suffix = T(' - for category %s') % cat.upper()
    config = dict(
        t_transactions_week_cat=dict(
            color='#ae36b9',
            label=T('My expenses / week') + desc_suffix),
        t_predictions_week_cat=dict(
            color='#2db965',
            label=T('My predicted future expenses / week') + desc_suffix))

    plots = []

    last_entry = __last_entry(CLIENT_REF)
    if not last_entry:
        return dict(
            title=T('My stats') + desc_suffix,
            content=T('Could not find last entry :('))

    for table_name in config:
        query = db[table_name].f_cat == cat
        if table_name == 't_transactions_week_cat':
            query &= db[table_name].f_date > (
                last_entry - timedelta(days=DAYS_SPAN))
        else:
            query &= (db[table_name].f_date > last_entry) & \
                (db[table_name].f_date < last_entry + timedelta(
                    days=DAYS_SPAN))
        rows = db(query)(
            db[table_name].client_ref == CLIENT_REF).select(
            orderby=db[table_name].f_date,
            cache=(cache.disk, auth.user.cache_timeout),
            cacheable=True)

        if len(rows) == 0:
            return dict(
                title=T('My stats'),
                content=T('Lack of sufficient amount of '
                          'data to render the plots :('))

        bar = Bar(
            pd.DataFrame(rows.as_list()),
            'f_date', values='f_expenses', legend=None,
            agg='sum', color=config[table_name]['color'],
            width=SWIDTH, height=SHEIGHT, sizing_mode='scale_width',
            xlabel=str(T('Week')),
            ylabel=str(T('Spent')) + ' [PLN]',
            title=str(config[table_name]['label']))

        plots.append(bar)

    return dict(
        title=T('My stats'),
        content=XML(file_html(column(
            plots, width=SWIDTH, height=SHEIGHT,
            sizing_mode='scale_width'), CDN)))


# @cache.action(time_expire=3600, cache_model=cache.disk, quick='LP')
def gstats():
    from bokeh.models.tickers import DatetimeTicker
    from bokeh.palettes import RdYlGn6 as palette

    plots = []

    avg = db.tm_fuel.f_price.avg()
    names = [r.f_name for r in
             db(db.tm_fuel).select(db.tm_fuel.f_name, distinct=True)]
    rows = db(db.tm_fuel).select(
        db.tm_fuel.f_date,
        db.tm_fuel.f_name,
        avg,
        groupby=db.tm_fuel.f_name | db.tm_fuel.f_date,
        orderby=db.tm_fuel.f_date | db.tm_fuel.f_name,
        cache=(cache.disk, 3600),
        cacheable=True)
    data = [dict(
        avg_price=r[avg],
        name=r.tm_fuel.f_name,
        date=r.tm_fuel.f_date) for r in rows]
    df = pd.DataFrame(data)
    keep = dict(name=['ON'])

    p = figure(plot_width=SWIDTH, plot_height=SHEIGHT, x_axis_type="datetime",
               title=str(T("Fuel price changes")))
    p.yaxis.axis_label = str(T('Price for liter [PLN]'))
    p.yaxis.axis_line_width = 3
    p.xaxis.axis_label = str(T("Datetime"))
    p.xaxis.axis_line_width = 3
    p.xaxis.ticker = DatetimeTicker(desired_num_ticks=24)

    colors_bank = ['red', 'green', 'blue', 'navy', 'magenta']
    for idx, n in enumerate(names):
        keep = dict(name=[n])
        _df = df[df[list(keep)].isin(keep).all(axis=1)]
        p.line(_df['date'], _df['avg_price'],
               color=colors_bank[idx], alpha=0.5,
               legend=n)
    plots.append(row(p, width=SWIDTH, height=SHEIGHT,
                     sizing_mode='scale_width'))

    sum = db.t_transactions_week_cat.f_expenses.sum()
    rows2 = db(db.t_transactions_week_cat).select(
        db.t_transactions_week_cat.f_cat,
        db.t_transactions_week_cat.client_ref,
        sum,
        groupby=(db.t_transactions_week_cat.f_cat |
                 db.t_transactions_week_cat.client_ref),
        cache=(cache.disk, 3600),
        cacheable=True)

    clients = dict()
    for u in db(
        db.auth_user.client_ref.belongs(
            np.unique([r.t_transactions_week_cat.client_ref for r in rows2]))
            ).select(
                db.auth_user.client_ref,
                db.auth_user.first_name,
                db.auth_user.last_name,
                cache=(cache.disk, 3600),
                cacheable=True):
        clients[u.client_ref] = '%(first_name)s %(last_name)s' % u

    data = []
    for r in rows2:
        _client_ref = r.t_transactions_week_cat.client_ref
        ukey = clients.get(_client_ref, str(_client_ref))
        data.append({
            'name': ukey,
            'cat': r.t_transactions_week_cat.f_cat,
            'sum': r[sum]
            })

    df2 = pd.DataFrame(data)

    hm = HeatMap(
        df2, x='cat', y='name', values='sum',
        palette=palette[::-1],
        stat=None, height=200 * len(clients),
        xlabel=str(T('Product categories')),
        ylabel=str(T('System users')),
        title=str(T('Matrix of expenses (all time)')))
    hm.legend.background_fill_alpha = .4

    plots.append(row(hm, width=SWIDTH, height=SHEIGHT,
                     sizing_mode='scale_width'))
    return dict(
        title=T('Global stats'),
        content=XML(file_html(
            column(*plots,
                   width=SWIDTH, height=SHEIGHT,
                   sizing_mode='scale_width'), CDN)))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
