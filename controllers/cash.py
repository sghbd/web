# -*- coding: utf-8 -*-
from kafka import KafkaProducer
from kafka import errors as err

try:
    producer = KafkaProducer(bootstrap_servers=myconf.get('db.kafka'))
except (err.KafkaTimeoutError, err.NoBrokersAvailable):
    raise HTTP(503,
               T('Kafka broker propably is down (timeout or not available)!'
                 ' Please turn it on! ;)'))


@auth.requires_login()
def index():
    import os
    import pyxb
    import eparagon as ep
    import _etd as etd
    from helpers import Form

    form = Form(auth.user.client_ref)

    # pyxb.utils.domutils.BindingDOMSupport.SetDefaultNamespace(
    #     ep.Namespace)
    # pyxb.utils.domutils.BindingDOMSupport.DeclareNamespace(
    #     etd.Namespace, 'etd')
    xml_template = os.path.join(
        request.folder, 'modules', 'eparagon_schema', 'example.xml')
    r = ep.CreateFromDocument(open(xml_template, 'r').read())

    # pyxb.RequireValidWhenGenerating(False)

    if form.accepts(request, session):
        try:
            # form.write2xml(request.vars, r)
            producer.send('EParagon-Test', form.write2xml(request.vars, r))
            response.flash = T('Form has been sent')
        except Exception as error:
            response.flash = T('Form validation failed!') + ' ' + str(error)
    elif form.errors:
        response.flash = T('Form has errors!')

    return dict(title=T('Cash register'), content=form)


def lastep():
    import os
    import gluon.contenttype
    filename = '/tmp/test.xml'
    response.headers['Content-Type'] = gluon.contenttype.contenttype(filename)
    pathfilename = os.path.join(filename)
    try:
        return response.stream(open(pathfilename, 'rb'), chunk_size=10**6)
    except IOError:
        raise HTTP(
            503,
            T('There is no such file. '
              'Maybe generator did not output eparagon to the file.'))
