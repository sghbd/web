# -*- coding: utf-8 -*-
import re
from datetime import date
import pyxb
from gluon import current
from gluon.html import *
from gluon.validators import *
from gluon.sqlhtml import *
from gluon.tools import addrow
from gluon.storage import Storage

from _etd import TKodUS, TKodKraju
enum_TKodUS = TKodUS._CF_enumeration.values
enum_TKodKraju = TKodKraju._CF_enumeration.values

CACHE_TIMEOUT = 7200
EXTRACT_TMPID = re.compile(r'_epprod_(\d+)')


class Form(FORM):

    WIDGETS = {
        'string': SQLFORM.widgets.string.widget,
        'text': SQLFORM.widgets.text.widget,
        'password': SQLFORM.widgets.password.widget,
        'integer': SQLFORM.widgets.integer.widget,
        'double': SQLFORM.widgets.double.widget,
        'time': SQLFORM.widgets.time.widget,
        'date': SQLFORM.widgets.date.widget,
        'datetime': SQLFORM.widgets.datetime.widget,
        'upload': SQLFORM.widgets.upload.widget,
        'boolean': SQLFORM.widgets.boolean.widget,
        'options': SQLFORM.widgets.options.widget,
        'multiple': SQLFORM.widgets.multiple.widget,
        'radio': SQLFORM.widgets.radio.widget,
        'checkboxes': SQLFORM.widgets.checkboxes.widget,
        'autocomplete': SQLFORM.widgets.autocomplete
        }

    @staticmethod
    def product_factory(id, cls):
        req = current.request
        db = current.db
        T = current.T
        tab_map = dict(
            tesco='tm_tesco',
            fuel='tm_fuel',
            hm='tm_hm')
        lang_map = dict(
            tesco=T('Product Tesco'),
            fuel=T('Fuel'),
            hm=T('Product H&M'))

        help_fields = None
        help_string = None
        # if cls != 'hm':
        #     help_fields = [db[tab_map[cls]].f_price, db[tab_map[cls]].f_unit]
        #     help_string = '%(f_name)s [%(f_price)sPLN/%(f_unit)s]'
        # else:
        #     help_fields = [db[tab_map[cls]].f_price]
        #     help_string = '%(f_name)s [%(f_price)sPLN]'

        w = AutocompleteWidget(
            req, db[tab_map[cls]].f_name, id_field=db[tab_map[cls]].id,
            help_fields=help_fields, help_string=help_string,
            limitby=(0, 30), min_length=1)
        return w(db.tm_tesco.id, 1, id,
                 plabel=lang_map[cls],
                 pclass=cls,
                 _id='_autocomplete_%s_epprod_%d' % (cls, id))

    def __init__(self, client_id, *components, **attributes):
        FORM.__init__(self, INPUT(_type='submit'),
                      _class='form-horizontal', *components, **attributes)

        def hr():
            return HR(_style='border-top: 2px solid #e2e2e2;')

        T = current.T
        fields = [
            Storage(
                name='Klient.IKN', type='integer',
                label=T('Client ID'), default=client_id, readonly=True,
                comment=T('You can edit this value in profile panel')),
            hr(),
            DIV(_id='eprod_holder', _class='row'),
            hr(),
            Storage(
                name='Naglowek.KodFormularza', type='string',
                label=None, default='EParagon', readonly=True),
            Storage(
                name='Naglowek.WariantFormularza', type='integer',
                label=None, default=1, readonly=True),
            Storage(
                name='Naglowek.CelZlozenia', type='integer',
                label=None, default=1, readonly=True),
            Storage(
                name='Naglowek.DataWytworzeniaEParagon', type='date',
                label=T('Creation date'), default=date.today(), readonly=False),
            Storage(
                name='Naglowek.DomyslnyKodWaluty', type='string',
                label=T('Currency'), default='PLN', readonly=True),
            Storage(
                name='Naglowek.KodUrzedu', type='select',
                options=enum_TKodUS(),
                label=T('Office code'), default=2416, readonly=False),
            hr(),
            Storage(
                name='Podatnik.IdentyfikatorPodmiotu.NIP', type='integer',
                label='NIP', default=7811897358, readonly=False,
                requires=IS_MATCH(
                    r'^\d{10}$', error_message=T('Not NIP code - 10 digits'))),
            Storage(
                name='Podatnik.IdentyfikatorPodmiotu.PelnaNazwa',
                type='string',
                label=T('Full name of the subject'),
                default='Lidl sp. z o.o. sp. k.', readonly=False),
            hr(),
            Storage(
                name='Podatnik.AdresPodmiotu.KodKraju', type='select',
                options=enum_TKodKraju(),
                label=T('Country code'), default='PL', readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.Wojewodztwo', type='string',
                label=T('Vovoidship'), default='mazowieckie', readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.Powiat', type='string',
                label=T('County'), default='poznański', readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.Gmina', type='string',
                label=T('Community'),
                default='Tarnowo Podgórne', readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.Ulica', type='string',
                label=T('Street'), default='Poznańska', readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.NrDomu', type='integer',
                label=T('House Nb'), default=48, readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.Miejscowosc', type='string',
                label=T('City'), default='Jankowice', readonly=False),
            Storage(
                name='Podatnik.AdresPodmiotu.KodPocztowy', type='string',
                label=T('Postal code'), default='62-080', readonly=False,
                requires=IS_MATCH(
                    r'^\d{2}-\d{3}$',
                    error_message=T('Wrong format, example: 12-123'))),
            Storage(
                name='Podatnik.AdresPodmiotu.Poczta', type='string',
                label=T('Post'),
                default='Tarnowo Podgórne', readonly=False),
            hr(),
            Storage(
                name='Podatnik.PunktSprzedazy.KodKraju', type='select',
                options=enum_TKodKraju(),
                label=T('Sales point') + ' ' + T('Country code'),
                default='PL', readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.Wojewodztwo', type='string',
                label=T('Sales point') + ' ' + T('Vovoidship'),
                default='dolnośląskie', readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.Powiat', type='string',
                label=T('Sales point') + ' ' + T('County'),
                default='wrocławski', readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.Gmina', type='string',
                label=T('Sales point') + ' ' + T('Community'),
                default='Wrocław', readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.Ulica', type='string',
                label=T('Sales point') + ' ' + T('Street'),
                default='Grabiszyńska', readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.NrDomu', type='integer',
                label=T('Sales point') + ' ' + T('House Nb'),
                default=253, readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.Miejscowosc', type='string',
                label=T('Sales point') + ' ' + T('City'),
                default='Wrocław', readonly=False),
            Storage(
                name='Podatnik.PunktSprzedazy.KodPocztowy', type='string',
                label=T('Sales point') + ' ' + T('Postal code'),
                default='53-234', readonly=False,
                requires=IS_MATCH(
                    r'^\d{2}-\d{3}$',
                    error_message=T('Wrong format, example: 12-123'))),
            Storage(
                name='Podatnik.PunktSprzedazy.Poczta', type='string',
                label=T('Sales point') + ' ' + T('Post'),
                default='Wrocław', readonly=False),
            hr(),
            Storage(
                name='ParagonFiskalny.DataWystawienia', type='date',
                label=T('Issue date'), default=date.today(), readonly=False),
            Storage(
                name='ParagonFiskalny.UnikatowyNrKasy', type='string',
                label=T('Currency'), default='BGI14143146', readonly=False),
            Storage(
                name='ParagonFiskalny.KolejnyNrWydruku', type='string',
                label=T('Print nb'), default='442092', readonly=False),
            Storage(
                name='ParagonFiskalny.NrKasy', type='string',
                label=T('Cash box nb'), default='0001', readonly=False),
            Storage(
                name='ParagonFiskalny.NrKasjera', type='integer',
                label=T('Cashier nb'), default=41, readonly=False)
            ]

        for idx, field in enumerate(fields):
            self.add(field)
            if idx == 2:
                button_div = DIV(
                    BUTTON(
                        T('Tesco +'), _id='first', _class='btn btn-primary',
                       _onclick='duplicateWidget(event, "tesco"); return false;',
                       _style='margin-top: 3px;'),
                    BUTTON(
                        'H&M +', _id='first', _class='btn btn-primary',
                       _onclick='duplicateWidget(event, "hm"); return false;',
                       _style='margin-top: 3px;'),
                    BUTTON(
                        T('Fuel +'), _id='first', _class='btn btn-primary',
                       _onclick='duplicateWidget(event, "fuel"); return false;',
                       _style='margin-top: 3px;'))
                self.insert(-1, button_div)
                w1 = Form.product_factory(1, 'tesco')
                w2 = Form.product_factory(2, 'fuel')
                w3 = Form.product_factory(3, 'hm')
                self[idx].insert(-1, w1)
                self[idx].insert(-1, w2)
                self[idx].insert(-1, w3)

    def script(self):
        return SCRIPT("""
            var atime = 100;
            var PROD_COUNT = 100;
            var EPRODREGX = new RegExp(/_epprod_\d+/mg);

            function duplicateWidget(event, cls) {
            console.log('.eprod.' + cls);
                if (event.x == 0 && event.y == 0) {
                    event.preventDefault();
                    return false;
                }
                PROD_COUNT++;
                var prod = $('.eprod.' + cls).last();
                var prod2 = prod.clone();
                prod2.find('.btn-danger').show();
                prod2 =  $.parseHTML(
                    prod2.html().replace(EPRODREGX, "_epprod_" + PROD_COUNT));
                var holder = $('<div></div>');
                holder.addClass('form-group').addClass('eprod');
                holder.append(prod2);
                $(holder).find('.form-control').removeClass('invalidinput');
                prod.parent().append(holder)
                return false;
            }

            function removeMe(event, but) {
                if (event.x == 0 && event.y == 0) {
                    event.preventDefault();
                    return false;
                }
                $(but).parent().remove();
                if ($('.eprod').length == 1) {
                    $('.eprod:gt(0) button.btn-danger').last().fadeOut(atime);
                } else if ($('.eprod').length == 2) {
                    $('.eprod:gt(0) button.btn-danger').each(function(id, elem) {
                        $(elem).show().fadeIn(atime);
                    });
                }
            }
        """)

    def group_n_append_products(self, vars, root, cls='tesco',
                                overwritefirst=False):
        table_name = 'tm_' + cls
        products_tmp = Storage()
        for k in [k for k in vars.keys()
                  if k.startswith('_epprod') and cls in k]:
            tmpid, key = k.split('__')
            product_key = EXTRACT_TMPID.search(tmpid).group(1)
            if not products_tmp[product_key]:
                products_tmp[product_key] = {}
            products_tmp[product_key][key] = vars[k]

        amount_k = '_autocomplete_tm_%s_f_name_aux_amount' % cls
        amounts = Storage()
        for p in products_tmp.values():
            id = p['id_%s' % cls]
            if id not in amounts:
                amounts[id] = float(p[amount_k])
            else:
                amounts[id] += float(p[amount_k])

        db = current.db
        rows = db(db[table_name].id.belongs(amounts.keys())).select()

        A_brutto = 0
        for idx, r in enumerate(rows):
            amount = amounts[str(r.id)]
            if amount == 0:
                continue
            try:
                unit = r.f_unit.strip()
            except AttributeError:
                unit = 'szt.'
            item = pyxb.BIND(
                P_1=r.f_name.decode('utf-8'),
                P_2=unit.upper(),
                P_3=str(amount),
                P_4=str(r.f_price),
                P_5=str(round(r.f_price * amount, 2)),
                # for simplicity we assume tax rate is always A
                P_6='A',
                typ='G')
            A_brutto += round(r.f_price * amount, 2)
            if overwritefirst:
                try:
                    root.ParagonWiersz[idx] = item
                except IndexError:
                    root.ParagonWiersz.append(item)
            else:
                root.ParagonWiersz.append(item)
        return A_brutto, len(rows)

    def write2xml(self, vars, root):
        from datetime import datetime
        import pprint
        pp = pprint.PrettyPrinter(indent=4)

        for k in vars.keys():
            vars[k] = vars[k].decode('utf-8')

        root.Klient.IKN = vars['Klient.IKN']
        root.Naglowek.CelZlozenia = vars['Naglowek.CelZlozenia']
        root.Naglowek.DataWytworzeniaEParagon = datetime.strptime(
            vars['Naglowek.DataWytworzeniaEParagon'], '%Y-%m-%d').isoformat()
        root.Naglowek.DomyslnyKodWaluty = vars['Naglowek.DomyslnyKodWaluty']
        # root.Naglowek.KodFormularza = vars['Naglowek.KodFormularza']
        root.Naglowek.WariantFormularza = vars['Naglowek.WariantFormularza']
        root.ParagonFiskalny.DataWystawienia = datetime.strptime(
        vars['ParagonFiskalny.DataWystawienia'], '%Y-%m-%d').isoformat()
        root.ParagonFiskalny.KolejnyNrWydruku = vars['ParagonFiskalny.KolejnyNrWydruku']
        root.ParagonFiskalny.NrKasjera = vars['ParagonFiskalny.NrKasjera']
        root.ParagonFiskalny.NrKasy = vars['ParagonFiskalny.NrKasy']
        root.ParagonFiskalny.UnikatowyNrKasy = vars['ParagonFiskalny.UnikatowyNrKasy']
        root.Podatnik.AdresPodmiotu.Gmina = vars['Podatnik.AdresPodmiotu.Gmina']
        root.Podatnik.AdresPodmiotu.KodPocztowy = vars['Podatnik.AdresPodmiotu.KodPocztowy']
        root.Podatnik.AdresPodmiotu.Miejscowosc = vars['Podatnik.AdresPodmiotu.Miejscowosc']
        root.Podatnik.AdresPodmiotu.NrDomu = vars['Podatnik.AdresPodmiotu.NrDomu']
        root.Podatnik.AdresPodmiotu.Poczta = vars['Podatnik.AdresPodmiotu.Poczta']
        root.Podatnik.AdresPodmiotu.Powiat = vars['Podatnik.AdresPodmiotu.Powiat']
        root.Podatnik.AdresPodmiotu.Ulica = vars['Podatnik.AdresPodmiotu.Ulica']
        root.Podatnik.AdresPodmiotu.Wojewodztwo = vars['Podatnik.AdresPodmiotu.Wojewodztwo']
        root.Podatnik.IdentyfikatorPodmiotu.NIP = vars['Podatnik.IdentyfikatorPodmiotu.NIP']
        root.Podatnik.IdentyfikatorPodmiotu.PelnaNazwa = vars['Podatnik.IdentyfikatorPodmiotu.PelnaNazwa']
        root.Podatnik.PunktSprzedazy.Gmina = vars['Podatnik.PunktSprzedazy.Gmina']
        root.Podatnik.PunktSprzedazy.KodPocztowy = vars['Podatnik.PunktSprzedazy.KodPocztowy']
        root.Podatnik.PunktSprzedazy.Miejscowosc = vars['Podatnik.PunktSprzedazy.Miejscowosc']
        root.Podatnik.PunktSprzedazy.NrDomu = vars['Podatnik.PunktSprzedazy.NrDomu']
        root.Podatnik.PunktSprzedazy.Poczta = vars['Podatnik.PunktSprzedazy.Poczta']
        root.Podatnik.PunktSprzedazy.Powiat = vars['Podatnik.PunktSprzedazy.Powiat']
        root.Podatnik.PunktSprzedazy.Ulica = vars['Podatnik.PunktSprzedazy.Ulica']
        root.Podatnik.PunktSprzedazy.Wojewodztwo = vars['Podatnik.PunktSprzedazy.Wojewodztwo']

        A_tax = .23
        product_counter = 0
        A_brutto = 0
        b1, c1 = self.group_n_append_products(vars, root, 'tesco', True)
        b2, c2 = self.group_n_append_products(vars, root, 'fuel')
        b3, c3 = self.group_n_append_products(vars, root, 'hm')
        product_counter = c1 + c2 + c3
        A_brutto = b1 + b2 + b3

        # for simplicity we assume tax rate is always A
        root.ParagonWierszCtrl.LiczbaWierszyParagonuPodatek_A = product_counter
        root.ParagonWierszCtrl.WartoscWierszyParagonu_A = str(A_brutto)
        root.ParagonWierszCtrl.WartoscPodatku_A = str(
            round(A_brutto * A_tax, 2))
        root.ParagonWierszCtrl.LiczbaWierszyParagonuPodatek_B = 1
        root.ParagonWierszCtrl.WartoscWierszyParagonu_B = 0
        root.ParagonWierszCtrl.WartoscPodatku_B = 0
        root.ParagonWierszCtrl.LiczbaWierszyParagonuPodatek_D = 1
        root.ParagonWierszCtrl.WartoscWierszyParagonu_D = 0
        root.ParagonWierszCtrl.WartoscPodatku_D = 0
        root.ParagonWierszCtrl.LacznaWartoscWierszyParagonu = str(A_brutto)
        root.ParagonWierszCtrl.LacznaWartoscPodatku = str(
            round(A_brutto * A_tax, 2))

        with open('/tmp/test.xml', 'w') as f:
            f.write(root.toDOM().toprettyxml().encode('utf-8'))
            # f.write(root.toxml('utf-8'))

        return root.toxml('utf-8')

    def add(self, item):
        if not isinstance(item, Storage):
            self.insert(-1, item)
            return
        if not item.label:
            self.insert(-1, INPUT(
                _name=item.name, _value=item.default, _type='hidden'))
            return
        if item.options:
            input = SELECT(
                item.options, value=item.default, _type='text',
                _class='form-control string %s' % item.type)
        else:
            input = INPUT(
                _name=item.name,
                _class='form-control %s' % item.type,
                _readonly=item.readonly,
                value=item.default,
                widget=Form.WIDGETS[item.type] if not item.readonly else None,
                requires=item.requires)
        addrow(
            [self], LABEL(item.label, _class='control-label'),
            input,
            item.comment if item.comment else '',
            'bootstrap3_inline', 'id_' + item.name)

    def xml(self):
        newform = FORM(*self.components, **self.attributes)
        hidden_fields = self.hidden_fields()
        if hidden_fields.components:
            newform.append(hidden_fields)
        return DIV.xml(CAT(self.script(), newform))


class AutocompleteWidget(object):
    _class = 'string'

    def __init__(self, request, field, id_field=None, db=None,
                 orderby=None, limitby=(0, 30), distinct=False,
                 keyword='_autocomplete_%(tablename)s_%(fieldname)s',
                 min_length=2, help_fields=None, help_string=None,
                 at_beginning=True):

        self.help_fields = help_fields or []
        self.help_string = help_string
        if self.help_fields and not self.help_string:
            self.help_string = ' '.join(
                '%%(%s)s' % f.name for f in self.help_fields)

        self.request = request
        self.keyword = keyword % dict(tablename=field.tablename,
                                      fieldname=field.name)
        self.db = db or field._db
        self.orderby = orderby
        self.limitby = limitby
        self.distinct = distinct
        self.min_length = min_length
        self.at_beginning = at_beginning
        self.fields = [field]
        if id_field:
            self.is_reference = True
            self.fields.append(id_field)
        else:
            self.is_reference = False
        if hasattr(request, 'application'):
            self.url = URL(args=request.args)
            self.callback()
        else:
            self.url = request

    def callback(self):
        if self.keyword in self.request.vars:
            field = self.fields[0]
            if type(field) is Field.Virtual:
                records = []
                table_rows = self.db(self.db[field.tablename]).select(
                    orderby=self.orderby)
                count = 0
                for row in table_rows:
                    if self.at_beginning:
                        if row[field.name].lower().startswith(
                                self.request.vars[self.keyword]):
                            count += 1
                            records.append(row)
                    else:
                        if self.request.vars[
                                self.keyword] in row[field.name].lower():
                            count += 1
                            records.append(row)
                    if count == 10:
                        break
                rows = Rows(self.db, records, table_rows.colnames,
                            compact=table_rows.compact)
            elif settings and settings.global_settings.web2py_runtime_gae:
                rows = self.db(field.__ge__(
                    self.request.vars[self.keyword]) & field.__lt__(
                        self.request.vars[self.keyword] + u'\ufffd')).select(
                            orderby=self.orderby,
                            limitby=self.limitby,
                            cache=(current.cache.disk, CACHE_TIMEOUT),
                            cacheable=True
                            *(self.fields + self.help_fields))
            elif self.at_beginning:
                rows = self.db(field.like(
                    '%' + self.request.vars[self.keyword] + '%',
                    case_sensitive=False
                    )).select(
                        orderby=self.orderby,
                        limitby=self.limitby,
                        distinct=self.distinct,
                        cache=(current.cache.disk, CACHE_TIMEOUT),
                        cacheable=True,
                        *(self.fields + self.help_fields))
            else:
                rows = self.db(field.contains(
                    self.request.vars[self.keyword],
                    case_sensitive=False)).select(
                        orderby=self.orderby,
                        limitby=self.limitby,
                        distinct=self.distinct,
                        cache=(current.cache.disk, CACHE_TIMEOUT),
                        cacheable=True,
                        *(self.fields + self.help_fields))
            if rows:
                if self.is_reference:
                    id_field = self.fields[1]
                    if self.help_fields:
                        options = [OPTION(
                            self.help_string % dict(
                                [(h.name, s[h.name]) for h
                                 in self.fields[:1] + self.help_fields]),
                            value=s[id_field.name],
                            _selected=(k == 0)) for k, s in enumerate(rows)]
                        print [s[id_field.name] for s in rows]
                    else:
                        options = [OPTION(
                            s[field.name], _value=s[id_field.name],
                            _selected=(k == 0)) for k, s in enumerate(rows)]
                    raise HTTP(
                        200, SELECT(_id=self.keyword, _class='autocomplete',
                                    _size=len(rows),
                                    _multiple=(len(rows) == 1),
                                    *options).xml())
                else:
                    raise HTTP(
                        200, SELECT(_id=self.keyword, _class='autocomplete',
                                    _size=len(rows),
                                    _multiple=(len(rows) == 1),
                                    *[OPTION(s[field.name],
                                             _selected=(k == 0))
                                      for k, s in enumerate(rows)]).xml())
            else:
                raise HTTP(200, '')

    def __call__(self, field, value, wid=None,
                 plabel='Product', pclass='', **attributes):
        default = dict(
            _type='text',
            value=(value is not None and str(value)) or '',
            )
        attr = StringWidget._attributes(field, default, **attributes)
        div_id = self.keyword + '_div'
        attr['_autocomplete'] = 'off'
        prefix = '' if not wid else '_epprod_%d__' % wid
        if self.is_reference:
            key2 = prefix + self.keyword + '_aux'
            key3 = prefix + self.keyword + '_auto'
            attr['_class'] = 'string form-control col-sm-8'
            #attr['_style'] = 'min-width: 50%;'
            name = prefix + attr['_name']
            if 'requires' in attr:
                del attr['requires']
            attr['_name'] = key2
            value = attr['value']
            if type(self.fields[0]) is Field.Virtual:
                record = None
                table_rows = self.db(
                    self.db[self.fields[0].tablename]).select(
                        orderby=self.orderby)
                for row in table_rows:
                    if row.id == value:
                        record = row
                        break
            else:
                record = self.db(
                    self.fields[1] == value).select(self.fields[0]).first()
            attr['value'] = record and record[self.fields[0].name]
            attr['_onblur'] = "jQuery('#%(div_id)s').delay(1000).fadeOut('fast');" % \
                dict(div_id=div_id, u='F' + self.keyword)
            attr['_onkeyup'] = """
var timer = null;
jQuery('#%(key3)s').val('');
var e=event.which?event.which:event.keyCode;
function %(u)s() {
    jQuery('#%(id)s').val(jQuery('#%(key)s :selected').text());
    jQuery('#%(key3)s').val(jQuery('#%(key)s').val())
};
if(e==39) %(u)s();
else if (e==40) {
    if(jQuery('#%(key)s option:selected').next().length) jQuery('#%(key)s option:selected')
        .attr('selected',null)
        .next()
        .attr('selected','selected');
    %(u)s();
} else if (e==38) {
    if(jQuery('#%(key)s option:selected').prev().length) jQuery('#%(key)s option:selected')
        .attr('selected',null)
        .prev()
        .attr('selected','selected');
    %(u)s();
} else if (jQuery('#%(id)s').val().length>=%(min_length)s && e == 13)
    jQuery.get('%(url)s?%(key)s='+encodeURIComponent(jQuery('#%(id)s').val()),function(data){if(data=='')jQuery('#%(key3)s').val('');
else {
    jQuery('#%(id)s').next('.error').hide();
    jQuery('#%(div_id)s').html(data).show().focus();
    jQuery('#%(div_id)s select').css('width',jQuery('#%(id)s').css('width'));
    jQuery('#%(key3)s').val(jQuery('#%(key)s').val());
    jQuery('#%(key)s').change(%(u)s);
    jQuery('#%(key)s').click(%(u)s);
};});
else jQuery('#%(div_id)s').fadeOut('slow');
            """ % \
                dict(url=self.url, min_length=self.min_length,
                     key=self.keyword, id=attr['_id'], key2=key2, key3=key3,
                     name=name, div_id=div_id, u='F' + self.keyword)
            if self.min_length == 0:
                attr['_onfocus'] = attr['_onkeyup']
            return DIV(
                LABEL(plabel, _class='control-label col-sm-1'),
                INPUT(**attr),
                LABEL('Ilość', _class='control-label col-sm-1'),
                INPUT(_name=attr['_name'] + '_amount', _type='text', _value=1,
                      _id=attr['_name'] + '_amount',
                      _class='form-control integer col-sm-1',
                      requires=IS_FLOAT_IN_RANGE(0, None),
                      widget=Form.WIDGETS['double']),
                BUTTON('-', _class='btn btn-danger',
                       _onclick='removeMe(event, this); return false;',
                       _style='margin-top: 3px; display: none;'),
                INPUT(_type='hidden', _id=key3, _value=value,
                      _name='%s_%s' % (name, pclass), requires=field.requires),
                DIV(_id=div_id, _style='position:absolute;'),
                _class='form-group eprod ' + pclass)
        else:
            attr['_name'] = field.name
            attr['_onblur'] = "jQuery('#%(div_id)s').delay(1000).fadeOut('fast');" % \
                dict(div_id=div_id, u='F' + self.keyword)
            attr['_onkeyup'] = """
var e=event.which?event.which:event.keyCode;
function %(u)s() { jQuery('#%(id)s').val(jQuery('#%(key)s').val()) };
if(e==39) %(u)s();
else if(e==40) {
    if(jQuery('#%(key)s option:selected').next().length)
        jQuery('#%(key)s option:selected')
            .attr('selected',null)
            .next()
            .attr('selected','selected');
    %(u)s();
} else if (e==38) {
    if(jQuery('#%(key)s option:selected').prev().length)
        jQuery('#%(key)s option:selected')
            .attr('selected',null)
            .prev()
            .attr('selected','selected');
    %(u)s();
} else if (jQuery('#%(id)s').val().length>=%(min_length)s && e == 13)
    jQuery.get('%(url)s?%(key)s='+encodeURIComponent(jQuery('#%(id)s').val()),function(data){
        jQuery('#%(id)s')
            .next('.error')
            .hide();
        jQuery('#%(div_id)s').html(data).show().focus();
        jQuery('#%(div_id)s select').css('width',jQuery('#%(id)s').css('width'));
        jQuery('#%(key)s').change(%(u)s);jQuery('#%(key)s').click(%(u)s);
    });
else jQuery('#%(div_id)s').fadeOut('slow');
            """ % \
                dict(url=self.url, min_length=self.min_length,
                     key=self.keyword, id=attr['_id'],
                     div_id=div_id,
                     u='F' + self.keyword)
            if self.min_length == 0:
                attr['_onfocus'] = attr['_onkeyup']
            return CAT(INPUT(**attr),
                       DIV(_id=div_id, _style='position:absolute;'))
