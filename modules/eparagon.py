# ./eparagon.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:f6dc490aae685cb16052f6c98bdffbe7d3803cde
# Generated 2017-08-31 21:14:28.863149 by PyXB version 1.2.4 using Python 2.7.12.final.0
# Namespace http://jpk.mf.gov.pl/wzor/2016/03/09/03095/

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six

# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:76e52a5e-8e80-11e7-9a3c-ebbb2271bf70')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.4'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# Import bindings for namespaces imported into schema
import _kck as _ImportedBinding__kck
import pyxb.binding.datatypes
import _etd as _ImportedBinding__etd

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://jpk.mf.gov.pl/wzor/2016/03/09/03095/', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TKodFormularza
class TKodFormularza (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """Symbol wzoru formularza"""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TKodFormularza')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 11, 1)
    _Documentation = 'Symbol wzoru formularza'
TKodFormularza._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TKodFormularza, enum_prefix=None)
TKodFormularza.EParagon = TKodFormularza._CF_enumeration.addEnumeration(unicode_value='EParagon', tag='EParagon')
TKodFormularza._InitializeFacetMap(TKodFormularza._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TKodFormularza', TKodFormularza)

# Atomic simple type: {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TCelZlozenia
class TCelZlozenia (pyxb.binding.datatypes.byte, pyxb.binding.basis.enumeration_mixin):

    """Określenie celu złożenia EParagon"""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TCelZlozenia')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 19, 1)
    _Documentation = 'Okre\u015blenie celu z\u0142o\u017cenia EParagon'
TCelZlozenia._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TCelZlozenia, enum_prefix=None)
TCelZlozenia._CF_enumeration.addEnumeration(unicode_value='1', tag=None)
TCelZlozenia._InitializeFacetMap(TCelZlozenia._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TCelZlozenia', TCelZlozenia)

# Atomic simple type: [anonymous]
class STD_ANON (pyxb.binding.datatypes.byte, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 47, 4)
    _Documentation = None
STD_ANON._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON, enum_prefix=None)
STD_ANON._CF_enumeration.addEnumeration(unicode_value='1', tag=None)
STD_ANON._InitializeFacetMap(STD_ANON._CF_enumeration)

# Atomic simple type: {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TKwotowy
class TKwotowy (pyxb.binding.datatypes.decimal):

    """Wartość numeryczna 18 znaków max, w tym 2 znaki po przecinku"""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TKwotowy')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 67, 1)
    _Documentation = 'Warto\u015b\u0107 numeryczna 18 znak\xf3w max, w tym 2 znaki po przecinku'
TKwotowy._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(2))
TKwotowy._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(18))
TKwotowy._InitializeFacetMap(TKwotowy._CF_fractionDigits,
   TKwotowy._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'TKwotowy', TKwotowy)

# Atomic simple type: {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TZnakowyJPK
class TZnakowyJPK (pyxb.binding.datatypes.token):

    """Typ znakowy ograniczony do 256 znaków"""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TZnakowyJPK')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 84, 1)
    _Documentation = 'Typ znakowy ograniczony do 256 znak\xf3w'
TZnakowyJPK._CF_minLength = pyxb.binding.facets.CF_minLength(value=pyxb.binding.datatypes.nonNegativeInteger(1))
TZnakowyJPK._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(256))
TZnakowyJPK._InitializeFacetMap(TZnakowyJPK._CF_minLength,
   TZnakowyJPK._CF_maxLength)
Namespace.addCategoryObject('typeBinding', 'TZnakowyJPK', TZnakowyJPK)

# Atomic simple type: {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TIlosciJPK
class TIlosciJPK (pyxb.binding.datatypes.decimal):

    """Wykorzystywany do określenia ilości. Wartość numeryczna 22 znaki max, w tym 6 po przecinku."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TIlosciJPK')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 93, 1)
    _Documentation = 'Wykorzystywany do okre\u015blenia ilo\u015bci. Warto\u015b\u0107 numeryczna 22 znaki max, w tym 6 po przecinku.'
TIlosciJPK._CF_fractionDigits = pyxb.binding.facets.CF_fractionDigits(value=pyxb.binding.datatypes.nonNegativeInteger(6))
TIlosciJPK._CF_totalDigits = pyxb.binding.facets.CF_totalDigits(value=pyxb.binding.datatypes.positiveInteger(22))
TIlosciJPK._InitializeFacetMap(TIlosciJPK._CF_fractionDigits,
   TIlosciJPK._CF_totalDigits)
Namespace.addCategoryObject('typeBinding', 'TIlosciJPK', TIlosciJPK)

# Atomic simple type: {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaturalnyJPK
class TNaturalnyJPK (_ImportedBinding__etd.TNaturalny):

    """Liczby naturalne większe od zera"""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TNaturalnyJPK')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 76, 1)
    _Documentation = 'Liczby naturalne wi\u0119ksze od zera'
TNaturalnyJPK._CF_minExclusive = pyxb.binding.facets.CF_minExclusive(value_datatype=_ImportedBinding__etd.TNaturalny, value=pyxb.binding.datatypes.integer(0))
TNaturalnyJPK._InitializeFacetMap(TNaturalnyJPK._CF_minExclusive)
Namespace.addCategoryObject('typeBinding', 'TNaturalnyJPK', TNaturalnyJPK)

# Atomic simple type: [anonymous]
class STD_ANON_ (TZnakowyJPK, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 255, 8)
    _Documentation = None
STD_ANON_._CF_maxLength = pyxb.binding.facets.CF_maxLength(value=pyxb.binding.datatypes.nonNegativeInteger(2))
STD_ANON_._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=STD_ANON_, enum_prefix=None)
STD_ANON_.A = STD_ANON_._CF_enumeration.addEnumeration(unicode_value='A', tag='A')
STD_ANON_.B = STD_ANON_._CF_enumeration.addEnumeration(unicode_value='B', tag='B')
STD_ANON_.C = STD_ANON_._CF_enumeration.addEnumeration(unicode_value='C', tag='C')
STD_ANON_.D = STD_ANON_._CF_enumeration.addEnumeration(unicode_value='D', tag='D')
STD_ANON_._InitializeFacetMap(STD_ANON_._CF_maxLength,
   STD_ANON_._CF_enumeration)

# Complex type {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek with content type ELEMENT_ONLY
class TNaglowek (pyxb.binding.basis.complexTypeDefinition):
    """Nagłówek EParagon"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TNaglowek')
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 31, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}KodFormularza uses Python identifier KodFormularza
    __KodFormularza = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'KodFormularza'), 'KodFormularza', '__httpjpk_mf_gov_plwzor2016030903095_TNaglowek_httpjpk_mf_gov_plwzor2016030903095KodFormularza', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 36, 3), )

    
    KodFormularza = property(__KodFormularza.value, __KodFormularza.set, None, None)

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WariantFormularza uses Python identifier WariantFormularza
    __WariantFormularza = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WariantFormularza'), 'WariantFormularza', '__httpjpk_mf_gov_plwzor2016030903095_TNaglowek_httpjpk_mf_gov_plwzor2016030903095WariantFormularza', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 46, 3), )

    
    WariantFormularza = property(__WariantFormularza.value, __WariantFormularza.set, None, None)

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}CelZlozenia uses Python identifier CelZlozenia
    __CelZlozenia = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'CelZlozenia'), 'CelZlozenia', '__httpjpk_mf_gov_plwzor2016030903095_TNaglowek_httpjpk_mf_gov_plwzor2016030903095CelZlozenia', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 53, 3), )

    
    CelZlozenia = property(__CelZlozenia.value, __CelZlozenia.set, None, None)

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}DataWytworzeniaEParagon uses Python identifier DataWytworzeniaEParagon
    __DataWytworzeniaEParagon = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DataWytworzeniaEParagon'), 'DataWytworzeniaEParagon', '__httpjpk_mf_gov_plwzor2016030903095_TNaglowek_httpjpk_mf_gov_plwzor2016030903095DataWytworzeniaEParagon', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 54, 3), )

    
    DataWytworzeniaEParagon = property(__DataWytworzeniaEParagon.value, __DataWytworzeniaEParagon.set, None, 'Data i czas wytworzenia EParagon')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}DomyslnyKodWaluty uses Python identifier DomyslnyKodWaluty
    __DomyslnyKodWaluty = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DomyslnyKodWaluty'), 'DomyslnyKodWaluty', '__httpjpk_mf_gov_plwzor2016030903095_TNaglowek_httpjpk_mf_gov_plwzor2016030903095DomyslnyKodWaluty', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 59, 3), )

    
    DomyslnyKodWaluty = property(__DomyslnyKodWaluty.value, __DomyslnyKodWaluty.set, None, 'Trzyliterowy kod lokalnej waluty (ISO-4217), domy\u015blny dla wytworzonego EParagon')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}KodUrzedu uses Python identifier KodUrzedu
    __KodUrzedu = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'KodUrzedu'), 'KodUrzedu', '__httpjpk_mf_gov_plwzor2016030903095_TNaglowek_httpjpk_mf_gov_plwzor2016030903095KodUrzedu', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 64, 3), )

    
    KodUrzedu = property(__KodUrzedu.value, __KodUrzedu.set, None, None)

    _ElementMap.update({
        __KodFormularza.name() : __KodFormularza,
        __WariantFormularza.name() : __WariantFormularza,
        __CelZlozenia.name() : __CelZlozenia,
        __DataWytworzeniaEParagon.name() : __DataWytworzeniaEParagon,
        __DomyslnyKodWaluty.name() : __DomyslnyKodWaluty,
        __KodUrzedu.name() : __KodUrzedu
    })
    _AttributeMap.update({
        
    })
Namespace.addCategoryObject('typeBinding', 'TNaglowek', TNaglowek)


# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON (pyxb.binding.basis.complexTypeDefinition):
    """Jednolity plik kontrolny dla EParagon"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 106, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Naglowek uses Python identifier Naglowek
    __Naglowek = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Naglowek'), 'Naglowek', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095Naglowek', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 108, 4), )

    
    Naglowek = property(__Naglowek.value, __Naglowek.set, None, 'Nag\u0142\xf3wek EParagon')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Podatnik uses Python identifier Podatnik
    __Podatnik = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Podatnik'), 'Podatnik', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095Podatnik', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 118, 4), )

    
    Podatnik = property(__Podatnik.value, __Podatnik.set, None, None)

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Klient uses Python identifier Klient
    __Klient = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Klient'), 'Klient', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095Klient', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 139, 4), )

    
    Klient = property(__Klient.value, __Klient.set, None, None)

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}ParagonFiskalny uses Python identifier ParagonFiskalny
    __ParagonFiskalny = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ParagonFiskalny'), 'ParagonFiskalny', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095ParagonFiskalny', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 150, 4), )

    
    ParagonFiskalny = property(__ParagonFiskalny.value, __ParagonFiskalny.set, None, None)

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}StawkiPodatku uses Python identifier StawkiPodatku
    __StawkiPodatku = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'StawkiPodatku'), 'StawkiPodatku', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095StawkiPodatku', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 186, 4), )

    
    StawkiPodatku = property(__StawkiPodatku.value, __StawkiPodatku.set, None, 'Zestawienie stawek podatku, w okresie kt\xf3rego dotyczy EParagon')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}ParagonWiersz uses Python identifier ParagonWiersz
    __ParagonWiersz = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ParagonWiersz'), 'ParagonWiersz', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095ParagonWiersz', True, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 220, 4), )

    
    ParagonWiersz = property(__ParagonWiersz.value, __ParagonWiersz.set, None, 'Szczeg\xf3\u0142owe pozycje paragonu')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}ParagonWierszCtrl uses Python identifier ParagonWierszCtrl
    __ParagonWierszCtrl = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ParagonWierszCtrl'), 'ParagonWierszCtrl', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_httpjpk_mf_gov_plwzor2016030903095ParagonWierszCtrl', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 269, 4), )

    
    ParagonWierszCtrl = property(__ParagonWierszCtrl.value, __ParagonWierszCtrl.set, None, 'Sumy kontrolne dla wierszy faktur')

    _ElementMap.update({
        __Naglowek.name() : __Naglowek,
        __Podatnik.name() : __Podatnik,
        __Klient.name() : __Klient,
        __ParagonFiskalny.name() : __ParagonFiskalny,
        __StawkiPodatku.name() : __StawkiPodatku,
        __ParagonWiersz.name() : __ParagonWiersz,
        __ParagonWierszCtrl.name() : __ParagonWierszCtrl
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_ (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 119, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}IdentyfikatorPodmiotu uses Python identifier IdentyfikatorPodmiotu
    __IdentyfikatorPodmiotu = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'IdentyfikatorPodmiotu'), 'IdentyfikatorPodmiotu', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON__httpjpk_mf_gov_plwzor2016030903095IdentyfikatorPodmiotu', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 121, 7), )

    
    IdentyfikatorPodmiotu = property(__IdentyfikatorPodmiotu.value, __IdentyfikatorPodmiotu.set, None, 'Dane identyfikuj\u0105ce podmiot')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}AdresPodmiotu uses Python identifier AdresPodmiotu
    __AdresPodmiotu = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AdresPodmiotu'), 'AdresPodmiotu', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON__httpjpk_mf_gov_plwzor2016030903095AdresPodmiotu', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 126, 7), )

    
    AdresPodmiotu = property(__AdresPodmiotu.value, __AdresPodmiotu.set, None, 'Adres podmiotu')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}PunktSprzedazy uses Python identifier PunktSprzedazy
    __PunktSprzedazy = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PunktSprzedazy'), 'PunktSprzedazy', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON__httpjpk_mf_gov_plwzor2016030903095PunktSprzedazy', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 131, 7), )

    
    PunktSprzedazy = property(__PunktSprzedazy.value, __PunktSprzedazy.set, None, 'Punkt sprzedazy')

    _ElementMap.update({
        __IdentyfikatorPodmiotu.name() : __IdentyfikatorPodmiotu,
        __AdresPodmiotu.name() : __AdresPodmiotu,
        __PunktSprzedazy.name() : __PunktSprzedazy
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_2 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 140, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}IKN uses Python identifier IKN
    __IKN = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'IKN'), 'IKN', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_2_httpjpk_mf_gov_plwzor2016030903095IKN', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 142, 7), )

    
    IKN = property(__IKN.value, __IKN.set, None, 'Indywidualny Kod Nabywcy (klienta)')

    _ElementMap.update({
        __IKN.name() : __IKN
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_3 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 151, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}DataWystawienia uses Python identifier DataWystawienia
    __DataWystawienia = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DataWystawienia'), 'DataWystawienia', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_3_httpjpk_mf_gov_plwzor2016030903095DataWystawienia', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 153, 7), )

    
    DataWystawienia = property(__DataWystawienia.value, __DataWystawienia.set, None, 'Data wystawienia paragonu fiskalnego')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}UnikatowyNrKasy uses Python identifier UnikatowyNrKasy
    __UnikatowyNrKasy = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'UnikatowyNrKasy'), 'UnikatowyNrKasy', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_3_httpjpk_mf_gov_plwzor2016030903095UnikatowyNrKasy', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 158, 7), )

    
    UnikatowyNrKasy = property(__UnikatowyNrKasy.value, __UnikatowyNrKasy.set, None, 'Indywidualny nr pami\u0119ci kasy, zarejestrowany w Urz\u0119dzie Skarbowym')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}KolejnyNrWydruku uses Python identifier KolejnyNrWydruku
    __KolejnyNrWydruku = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'KolejnyNrWydruku'), 'KolejnyNrWydruku', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_3_httpjpk_mf_gov_plwzor2016030903095KolejnyNrWydruku', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 163, 7), )

    
    KolejnyNrWydruku = property(__KolejnyNrWydruku.value, __KolejnyNrWydruku.set, None, 'Kolejny nr wydruku danej kasy fiskalnej')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}NrParagonu uses Python identifier NrParagonu
    __NrParagonu = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'NrParagonu'), 'NrParagonu', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_3_httpjpk_mf_gov_plwzor2016030903095NrParagonu', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 168, 7), )

    
    NrParagonu = property(__NrParagonu.value, __NrParagonu.set, None, 'Nr paragonu danej kasy fiskalnej')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}NrKasy uses Python identifier NrKasy
    __NrKasy = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'NrKasy'), 'NrKasy', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_3_httpjpk_mf_gov_plwzor2016030903095NrKasy', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 173, 7), )

    
    NrKasy = property(__NrKasy.value, __NrKasy.set, None, 'Numer kasy (w danym punkcie sprzeda\u017cy)')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}NrKasjera uses Python identifier NrKasjera
    __NrKasjera = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'NrKasjera'), 'NrKasjera', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_3_httpjpk_mf_gov_plwzor2016030903095NrKasjera', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 178, 7), )

    
    NrKasjera = property(__NrKasjera.value, __NrKasjera.set, None, 'Indywidualny nr pami\u0119ci kasy, zarejestrowany w Urz\u0119dzie Skarbowym')

    _ElementMap.update({
        __DataWystawienia.name() : __DataWystawienia,
        __UnikatowyNrKasy.name() : __UnikatowyNrKasy,
        __KolejnyNrWydruku.name() : __KolejnyNrWydruku,
        __NrParagonu.name() : __NrParagonu,
        __NrKasy.name() : __NrKasy,
        __NrKasjera.name() : __NrKasjera
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_4 (pyxb.binding.basis.complexTypeDefinition):
    """Zestawienie stawek podatku, w okresie którego dotyczy EParagon"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 190, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Stawka1 uses Python identifier Stawka1
    __Stawka1 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Stawka1'), 'Stawka1', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_4_httpjpk_mf_gov_plwzor2016030903095Stawka1', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 192, 7), )

    
    Stawka1 = property(__Stawka1.value, __Stawka1.set, None, 'Warto\u015b\u0107 stawki podstawowej - A')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Stawka2 uses Python identifier Stawka2
    __Stawka2 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Stawka2'), 'Stawka2', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_4_httpjpk_mf_gov_plwzor2016030903095Stawka2', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 197, 7), )

    
    Stawka2 = property(__Stawka2.value, __Stawka2.set, None, 'Warto\u015b\u0107 stawki obni\u017conej pierwszej - B')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Stawka3 uses Python identifier Stawka3
    __Stawka3 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Stawka3'), 'Stawka3', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_4_httpjpk_mf_gov_plwzor2016030903095Stawka3', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 202, 7), )

    
    Stawka3 = property(__Stawka3.value, __Stawka3.set, None, 'Warto\u015b\u0107 stawki obni\u017conej drugiej - D')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Stawka4 uses Python identifier Stawka4
    __Stawka4 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Stawka4'), 'Stawka4', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_4_httpjpk_mf_gov_plwzor2016030903095Stawka4', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 207, 7), )

    
    Stawka4 = property(__Stawka4.value, __Stawka4.set, None, 'Warto\u015b\u0107 stawki obni\u017conej trzeciej - pole rezerwowe')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}Stawka5 uses Python identifier Stawka5
    __Stawka5 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Stawka5'), 'Stawka5', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_4_httpjpk_mf_gov_plwzor2016030903095Stawka5', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 212, 7), )

    
    Stawka5 = property(__Stawka5.value, __Stawka5.set, None, 'Warto\u015b\u0107 stawki obni\u017conej czwartej - pole rezerwowe')

    _ElementMap.update({
        __Stawka1.name() : __Stawka1,
        __Stawka2.name() : __Stawka2,
        __Stawka3.name() : __Stawka3,
        __Stawka4.name() : __Stawka4,
        __Stawka5.name() : __Stawka5
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_5 (pyxb.binding.basis.complexTypeDefinition):
    """Szczegółowe pozycje paragonu"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 224, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}P_1 uses Python identifier P_1
    __P_1 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'P_1'), 'P_1', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_httpjpk_mf_gov_plwzor2016030903095P_1', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 226, 7), )

    
    P_1 = property(__P_1.value, __P_1.set, None, 'Nazwa (rodzaj) towaru lub us\u0142ugi.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}P_2 uses Python identifier P_2
    __P_2 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'P_2'), 'P_2', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_httpjpk_mf_gov_plwzor2016030903095P_2', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 231, 7), )

    
    P_2 = property(__P_2.value, __P_2.set, None, 'Miara dostarczonych towar\xf3w lub zakres wykonanych us\u0142ug.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}P_3 uses Python identifier P_3
    __P_3 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'P_3'), 'P_3', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_httpjpk_mf_gov_plwzor2016030903095P_3', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 236, 7), )

    
    P_3 = property(__P_3.value, __P_3.set, None, 'Ilo\u015b\u0107 (liczba) dostarczonych towar\xf3w lub zakres wykonanych us\u0142ug.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}P_4 uses Python identifier P_4
    __P_4 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'P_4'), 'P_4', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_httpjpk_mf_gov_plwzor2016030903095P_4', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 241, 7), )

    
    P_4 = property(__P_4.value, __P_4.set, None, 'Cena wraz z kwot\u0105 podatku (cena jednostkowa brutto)')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}P_5 uses Python identifier P_5
    __P_5 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'P_5'), 'P_5', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_httpjpk_mf_gov_plwzor2016030903095P_5', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 246, 7), )

    
    P_5 = property(__P_5.value, __P_5.set, None, 'Warto\u015b\u0107 sprzeda\u017cy brutto')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}P_6 uses Python identifier P_6
    __P_6 = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'P_6'), 'P_6', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_httpjpk_mf_gov_plwzor2016030903095P_6', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 251, 7), )

    
    P_6 = property(__P_6.value, __P_6.set, None, 'Stawka podatku.')

    
    # Attribute typ uses Python identifier typ
    __typ = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'typ'), 'typ', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_5_typ', pyxb.binding.datatypes.anySimpleType, fixed=True, unicode_default='G', required=True)
    __typ._DeclarationLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 266, 6)
    __typ._UseLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 266, 6)
    
    typ = property(__typ.value, __typ.set, None, None)

    _ElementMap.update({
        __P_1.name() : __P_1,
        __P_2.name() : __P_2,
        __P_3.name() : __P_3,
        __P_4.name() : __P_4,
        __P_5.name() : __P_5,
        __P_6.name() : __P_6
    })
    _AttributeMap.update({
        __typ.name() : __typ
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_6 (pyxb.binding.basis.complexTypeDefinition):
    """Sumy kontrolne dla wierszy faktur"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 273, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}LiczbaWierszyParagonuPodatek_A uses Python identifier LiczbaWierszyParagonuPodatek_A
    __LiczbaWierszyParagonuPodatek_A = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_A'), 'LiczbaWierszyParagonuPodatek_A', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095LiczbaWierszyParagonuPodatek_A', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 275, 7), )

    
    LiczbaWierszyParagonuPodatek_A = property(__LiczbaWierszyParagonuPodatek_A.value, __LiczbaWierszyParagonuPodatek_A.set, None, 'Liczba wierszy paragonu, dla skali podatkowej D.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscWierszyParagonu_A uses Python identifier WartoscWierszyParagonu_A
    __WartoscWierszyParagonu_A = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_A'), 'WartoscWierszyParagonu_A', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscWierszyParagonu_A', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 280, 7), )

    
    WartoscWierszyParagonu_A = property(__WartoscWierszyParagonu_A.value, __WartoscWierszyParagonu_A.set, None, '\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej A.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscPodatku_A uses Python identifier WartoscPodatku_A
    __WartoscPodatku_A = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_A'), 'WartoscPodatku_A', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscPodatku_A', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 285, 7), )

    
    WartoscPodatku_A = property(__WartoscPodatku_A.value, __WartoscPodatku_A.set, None, '\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej A.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}LiczbaWierszyParagonuPodatek_B uses Python identifier LiczbaWierszyParagonuPodatek_B
    __LiczbaWierszyParagonuPodatek_B = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_B'), 'LiczbaWierszyParagonuPodatek_B', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095LiczbaWierszyParagonuPodatek_B', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 290, 7), )

    
    LiczbaWierszyParagonuPodatek_B = property(__LiczbaWierszyParagonuPodatek_B.value, __LiczbaWierszyParagonuPodatek_B.set, None, 'Liczba wierszy paragonu, dla skali podatkowej D.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscWierszyParagonu_B uses Python identifier WartoscWierszyParagonu_B
    __WartoscWierszyParagonu_B = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_B'), 'WartoscWierszyParagonu_B', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscWierszyParagonu_B', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 295, 7), )

    
    WartoscWierszyParagonu_B = property(__WartoscWierszyParagonu_B.value, __WartoscWierszyParagonu_B.set, None, '\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej B.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscPodatku_B uses Python identifier WartoscPodatku_B
    __WartoscPodatku_B = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_B'), 'WartoscPodatku_B', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscPodatku_B', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 300, 7), )

    
    WartoscPodatku_B = property(__WartoscPodatku_B.value, __WartoscPodatku_B.set, None, '\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej B.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}LiczbaWierszyParagonuPodatek_C uses Python identifier LiczbaWierszyParagonuPodatek_C
    __LiczbaWierszyParagonuPodatek_C = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_C'), 'LiczbaWierszyParagonuPodatek_C', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095LiczbaWierszyParagonuPodatek_C', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 305, 7), )

    
    LiczbaWierszyParagonuPodatek_C = property(__LiczbaWierszyParagonuPodatek_C.value, __LiczbaWierszyParagonuPodatek_C.set, None, 'Liczba wierszy paragonu, dla skali podatkowej D.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscWierszyParagonu_C uses Python identifier WartoscWierszyParagonu_C
    __WartoscWierszyParagonu_C = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_C'), 'WartoscWierszyParagonu_C', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscWierszyParagonu_C', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 310, 7), )

    
    WartoscWierszyParagonu_C = property(__WartoscWierszyParagonu_C.value, __WartoscWierszyParagonu_C.set, None, '\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej C.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscPodatku_C uses Python identifier WartoscPodatku_C
    __WartoscPodatku_C = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_C'), 'WartoscPodatku_C', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscPodatku_C', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 315, 7), )

    
    WartoscPodatku_C = property(__WartoscPodatku_C.value, __WartoscPodatku_C.set, None, '\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej C.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}LiczbaWierszyParagonuPodatek_D uses Python identifier LiczbaWierszyParagonuPodatek_D
    __LiczbaWierszyParagonuPodatek_D = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_D'), 'LiczbaWierszyParagonuPodatek_D', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095LiczbaWierszyParagonuPodatek_D', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 320, 7), )

    
    LiczbaWierszyParagonuPodatek_D = property(__LiczbaWierszyParagonuPodatek_D.value, __LiczbaWierszyParagonuPodatek_D.set, None, 'Liczba wierszy paragonu, dla skali podatkowej D.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscWierszyParagonu_D uses Python identifier WartoscWierszyParagonu_D
    __WartoscWierszyParagonu_D = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_D'), 'WartoscWierszyParagonu_D', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscWierszyParagonu_D', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 325, 7), )

    
    WartoscWierszyParagonu_D = property(__WartoscWierszyParagonu_D.value, __WartoscWierszyParagonu_D.set, None, '\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej D.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WartoscPodatku_D uses Python identifier WartoscPodatku_D
    __WartoscPodatku_D = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_D'), 'WartoscPodatku_D', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095WartoscPodatku_D', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 330, 7), )

    
    WartoscPodatku_D = property(__WartoscPodatku_D.value, __WartoscPodatku_D.set, None, '\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej D.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}LacznaWartoscWierszyParagonu uses Python identifier LacznaWartoscWierszyParagonu
    __LacznaWartoscWierszyParagonu = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'LacznaWartoscWierszyParagonu'), 'LacznaWartoscWierszyParagonu', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095LacznaWartoscWierszyParagonu', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 335, 7), )

    
    LacznaWartoscWierszyParagonu = property(__LacznaWartoscWierszyParagonu.value, __LacznaWartoscWierszyParagonu.set, None, '\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla wszystkich skal podatkowych. Kwota do zap\u0142aty.')

    
    # Element {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}LacznaWartoscPodatku uses Python identifier LacznaWartoscPodatku
    __LacznaWartoscPodatku = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'LacznaWartoscPodatku'), 'LacznaWartoscPodatku', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_6_httpjpk_mf_gov_plwzor2016030903095LacznaWartoscPodatku', False, pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 340, 7), )

    
    LacznaWartoscPodatku = property(__LacznaWartoscPodatku.value, __LacznaWartoscPodatku.set, None, '\u0141\u0105czna warto\u015b\u0107 podatku dla wszystkich skal podatkowych.')

    _ElementMap.update({
        __LiczbaWierszyParagonuPodatek_A.name() : __LiczbaWierszyParagonuPodatek_A,
        __WartoscWierszyParagonu_A.name() : __WartoscWierszyParagonu_A,
        __WartoscPodatku_A.name() : __WartoscPodatku_A,
        __LiczbaWierszyParagonuPodatek_B.name() : __LiczbaWierszyParagonuPodatek_B,
        __WartoscWierszyParagonu_B.name() : __WartoscWierszyParagonu_B,
        __WartoscPodatku_B.name() : __WartoscPodatku_B,
        __LiczbaWierszyParagonuPodatek_C.name() : __LiczbaWierszyParagonuPodatek_C,
        __WartoscWierszyParagonu_C.name() : __WartoscWierszyParagonu_C,
        __WartoscPodatku_C.name() : __WartoscPodatku_C,
        __LiczbaWierszyParagonuPodatek_D.name() : __LiczbaWierszyParagonuPodatek_D,
        __WartoscWierszyParagonu_D.name() : __WartoscWierszyParagonu_D,
        __WartoscPodatku_D.name() : __WartoscPodatku_D,
        __LacznaWartoscWierszyParagonu.name() : __LacznaWartoscWierszyParagonu,
        __LacznaWartoscPodatku.name() : __LacznaWartoscPodatku
    })
    _AttributeMap.update({
        
    })



# Complex type [anonymous] with content type SIMPLE
class CTD_ANON_7 (pyxb.binding.basis.complexTypeDefinition):
    """Complex type [anonymous] with content type SIMPLE"""
    _TypeDefinition = TKodFormularza
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_SIMPLE
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 37, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is TKodFormularza
    
    # Attribute kodSystemowy uses Python identifier kodSystemowy
    __kodSystemowy = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'kodSystemowy'), 'kodSystemowy', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_7_kodSystemowy', pyxb.binding.datatypes.string, fixed=True, unicode_default='EParagon (1)', required=True)
    __kodSystemowy._DeclarationLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 40, 7)
    __kodSystemowy._UseLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 40, 7)
    
    kodSystemowy = property(__kodSystemowy.value, __kodSystemowy.set, None, None)

    
    # Attribute wersjaSchemy uses Python identifier wersjaSchemy
    __wersjaSchemy = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'wersjaSchemy'), 'wersjaSchemy', '__httpjpk_mf_gov_plwzor2016030903095_CTD_ANON_7_wersjaSchemy', pyxb.binding.datatypes.string, fixed=True, unicode_default='1-0', required=True)
    __wersjaSchemy._DeclarationLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 41, 7)
    __wersjaSchemy._UseLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 41, 7)
    
    wersjaSchemy = property(__wersjaSchemy.value, __wersjaSchemy.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __kodSystemowy.name() : __kodSystemowy,
        __wersjaSchemy.name() : __wersjaSchemy
    })



# Complex type [anonymous] with content type ELEMENT_ONLY
class CTD_ANON_8 (TNaglowek):
    """Nagłówek EParagon"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 112, 5)
    _ElementMap = TNaglowek._ElementMap.copy()
    _AttributeMap = TNaglowek._AttributeMap.copy()
    # Base type is TNaglowek
    
    # Element KodFormularza ({http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}KodFormularza) inherited from {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek
    
    # Element WariantFormularza ({http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}WariantFormularza) inherited from {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek
    
    # Element CelZlozenia ({http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}CelZlozenia) inherited from {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek
    
    # Element DataWytworzeniaEParagon ({http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}DataWytworzeniaEParagon) inherited from {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek
    
    # Element DomyslnyKodWaluty ({http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}DomyslnyKodWaluty) inherited from {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek
    
    # Element KodUrzedu ({http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}KodUrzedu) inherited from {http://jpk.mf.gov.pl/wzor/2016/03/09/03095/}TNaglowek
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })



EParagon = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'EParagon'), CTD_ANON, documentation='Jednolity plik kontrolny dla EParagon', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 102, 1))
Namespace.addCategoryObject('elementBinding', EParagon.name().localName(), EParagon)



TNaglowek._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'KodFormularza'), CTD_ANON_7, scope=TNaglowek, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 36, 3)))

TNaglowek._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WariantFormularza'), STD_ANON, scope=TNaglowek, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 46, 3)))

TNaglowek._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'CelZlozenia'), TCelZlozenia, scope=TNaglowek, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 53, 3)))

TNaglowek._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DataWytworzeniaEParagon'), _ImportedBinding__etd.TDataCzas, scope=TNaglowek, documentation='Data i czas wytworzenia EParagon', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 54, 3)))

TNaglowek._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DomyslnyKodWaluty'), _ImportedBinding__kck.currCode_Type, scope=TNaglowek, documentation='Trzyliterowy kod lokalnej waluty (ISO-4217), domy\u015blny dla wytworzonego EParagon', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 59, 3)))

TNaglowek._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'KodUrzedu'), _ImportedBinding__etd.TKodUS, scope=TNaglowek, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 64, 3)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TNaglowek._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'KodFormularza')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 36, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TNaglowek._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WariantFormularza')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 46, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TNaglowek._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'CelZlozenia')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 53, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TNaglowek._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DataWytworzeniaEParagon')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 54, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TNaglowek._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DomyslnyKodWaluty')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 59, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TNaglowek._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'KodUrzedu')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 64, 3))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TNaglowek._Automaton = _BuildAutomaton()




CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Naglowek'), CTD_ANON_8, scope=CTD_ANON, documentation='Nag\u0142\xf3wek EParagon', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 108, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Podatnik'), CTD_ANON_, scope=CTD_ANON, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 118, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Klient'), CTD_ANON_2, scope=CTD_ANON, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 139, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ParagonFiskalny'), CTD_ANON_3, scope=CTD_ANON, location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 150, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'StawkiPodatku'), CTD_ANON_4, scope=CTD_ANON, documentation='Zestawienie stawek podatku, w okresie kt\xf3rego dotyczy EParagon', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 186, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ParagonWiersz'), CTD_ANON_5, scope=CTD_ANON, documentation='Szczeg\xf3\u0142owe pozycje paragonu', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 220, 4)))

CTD_ANON._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ParagonWierszCtrl'), CTD_ANON_6, scope=CTD_ANON, documentation='Sumy kontrolne dla wierszy faktur', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 269, 4)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Naglowek')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 108, 4))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Podatnik')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 118, 4))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Klient')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 139, 4))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ParagonFiskalny')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 150, 4))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'StawkiPodatku')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 186, 4))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ParagonWiersz')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 220, 4))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ParagonWierszCtrl')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 269, 4))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON._Automaton = _BuildAutomaton_()




CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'IdentyfikatorPodmiotu'), _ImportedBinding__etd.TIdentyfikatorOsobyNiefizycznej, scope=CTD_ANON_, documentation='Dane identyfikuj\u0105ce podmiot', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 121, 7)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AdresPodmiotu'), _ImportedBinding__etd.TAdresPolski, scope=CTD_ANON_, documentation='Adres podmiotu', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 126, 7)))

CTD_ANON_._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PunktSprzedazy'), _ImportedBinding__etd.TAdresPolski, scope=CTD_ANON_, documentation='Punkt sprzedazy', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 131, 7)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 131, 7))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'IdentyfikatorPodmiotu')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 121, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AdresPodmiotu')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 126, 7))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PunktSprzedazy')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 131, 7))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_._Automaton = _BuildAutomaton_2()




CTD_ANON_2._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'IKN'), TNaturalnyJPK, scope=CTD_ANON_2, documentation='Indywidualny Kod Nabywcy (klienta)', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 142, 7)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_2._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'IKN')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 142, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_2._Automaton = _BuildAutomaton_3()




CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DataWystawienia'), _ImportedBinding__etd.TDataCzas, scope=CTD_ANON_3, documentation='Data wystawienia paragonu fiskalnego', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 153, 7)))

CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'UnikatowyNrKasy'), TZnakowyJPK, scope=CTD_ANON_3, documentation='Indywidualny nr pami\u0119ci kasy, zarejestrowany w Urz\u0119dzie Skarbowym', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 158, 7)))

CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'KolejnyNrWydruku'), TNaturalnyJPK, scope=CTD_ANON_3, documentation='Kolejny nr wydruku danej kasy fiskalnej', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 163, 7)))

CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'NrParagonu'), TNaturalnyJPK, scope=CTD_ANON_3, documentation='Nr paragonu danej kasy fiskalnej', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 168, 7)))

CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'NrKasy'), TNaturalnyJPK, scope=CTD_ANON_3, documentation='Numer kasy (w danym punkcie sprzeda\u017cy)', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 173, 7)))

CTD_ANON_3._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'NrKasjera'), TNaturalnyJPK, scope=CTD_ANON_3, documentation='Indywidualny nr pami\u0119ci kasy, zarejestrowany w Urz\u0119dzie Skarbowym', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 178, 7)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DataWystawienia')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 153, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'UnikatowyNrKasy')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 158, 7))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'KolejnyNrWydruku')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 163, 7))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'NrParagonu')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 168, 7))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'NrKasy')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 173, 7))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_3._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'NrKasjera')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 178, 7))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_3._Automaton = _BuildAutomaton_4()




CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Stawka1'), _ImportedBinding__etd.TProcentowy, scope=CTD_ANON_4, documentation='Warto\u015b\u0107 stawki podstawowej - A', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 192, 7), unicode_default='0.23'))

CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Stawka2'), _ImportedBinding__etd.TProcentowy, scope=CTD_ANON_4, documentation='Warto\u015b\u0107 stawki obni\u017conej pierwszej - B', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 197, 7), unicode_default='0.08'))

CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Stawka3'), _ImportedBinding__etd.TProcentowy, scope=CTD_ANON_4, documentation='Warto\u015b\u0107 stawki obni\u017conej drugiej - D', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 202, 7), unicode_default='0.05'))

CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Stawka4'), _ImportedBinding__etd.TProcentowy, scope=CTD_ANON_4, documentation='Warto\u015b\u0107 stawki obni\u017conej trzeciej - pole rezerwowe', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 207, 7), unicode_default='0.00'))

CTD_ANON_4._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Stawka5'), _ImportedBinding__etd.TProcentowy, scope=CTD_ANON_4, documentation='Warto\u015b\u0107 stawki obni\u017conej czwartej - pole rezerwowe', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 212, 7), unicode_default='0.00'))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Stawka1')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 192, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Stawka2')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 197, 7))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Stawka3')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 202, 7))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Stawka4')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 207, 7))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_4._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Stawka5')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 212, 7))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_4._Automaton = _BuildAutomaton_5()




CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'P_1'), TZnakowyJPK, scope=CTD_ANON_5, documentation='Nazwa (rodzaj) towaru lub us\u0142ugi.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 226, 7)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'P_2'), TZnakowyJPK, scope=CTD_ANON_5, documentation='Miara dostarczonych towar\xf3w lub zakres wykonanych us\u0142ug.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 231, 7)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'P_3'), TIlosciJPK, scope=CTD_ANON_5, documentation='Ilo\u015b\u0107 (liczba) dostarczonych towar\xf3w lub zakres wykonanych us\u0142ug.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 236, 7)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'P_4'), TKwotowy, scope=CTD_ANON_5, documentation='Cena wraz z kwot\u0105 podatku (cena jednostkowa brutto)', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 241, 7)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'P_5'), TKwotowy, scope=CTD_ANON_5, documentation='Warto\u015b\u0107 sprzeda\u017cy brutto', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 246, 7)))

CTD_ANON_5._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'P_6'), STD_ANON_, scope=CTD_ANON_5, documentation='Stawka podatku.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 251, 7)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 226, 7))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 231, 7))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 236, 7))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 241, 7))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 246, 7))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 251, 7))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'P_1')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 226, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'P_2')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 231, 7))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'P_3')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 236, 7))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'P_4')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 241, 7))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'P_5')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 246, 7))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_5._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'P_6')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 251, 7))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_5._Automaton = _BuildAutomaton_6()




CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_A'), TNaturalnyJPK, scope=CTD_ANON_6, documentation='Liczba wierszy paragonu, dla skali podatkowej D.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 275, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_A'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej A.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 280, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_A'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej A.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 285, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_B'), TNaturalnyJPK, scope=CTD_ANON_6, documentation='Liczba wierszy paragonu, dla skali podatkowej D.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 290, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_B'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej B.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 295, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_B'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej B.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 300, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_C'), TNaturalnyJPK, scope=CTD_ANON_6, documentation='Liczba wierszy paragonu, dla skali podatkowej D.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 305, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_C'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej C.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 310, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_C'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej C.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 315, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_D'), TNaturalnyJPK, scope=CTD_ANON_6, documentation='Liczba wierszy paragonu, dla skali podatkowej D.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 320, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_D'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla skali podatkowej D.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 325, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_D'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 podatku dla skali podatkowej D.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 330, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'LacznaWartoscWierszyParagonu'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 kolumny P_5 tabeli ParagonWiersz, dla wszystkich skal podatkowych. Kwota do zap\u0142aty.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 335, 7)))

CTD_ANON_6._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'LacznaWartoscPodatku'), TKwotowy, scope=CTD_ANON_6, documentation='\u0141\u0105czna warto\u015b\u0107 podatku dla wszystkich skal podatkowych.', location=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 340, 7)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 275, 7))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 280, 7))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 285, 7))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 290, 7))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 295, 7))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 300, 7))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 305, 7))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 310, 7))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 315, 7))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 320, 7))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 325, 7))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 330, 7))
    counters.add(cc_11)
    cc_12 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 335, 7))
    counters.add(cc_12)
    cc_13 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 340, 7))
    counters.add(cc_13)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_A')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 275, 7))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_A')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 280, 7))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_A')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 285, 7))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_B')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 290, 7))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_B')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 295, 7))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_B')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 300, 7))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_C')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 305, 7))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_C')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 310, 7))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_C')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 315, 7))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'LiczbaWierszyParagonuPodatek_D')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 320, 7))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscWierszyParagonu_D')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 325, 7))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WartoscPodatku_D')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 330, 7))
    st_11 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_12, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'LacznaWartoscWierszyParagonu')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 335, 7))
    st_12 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_12)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_13, False))
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_6._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'LacznaWartoscPodatku')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 340, 7))
    st_13 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_13)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_10, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_11, True) ]))
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_11, False) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_11, False) ]))
    st_11._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_12, [
        fac.UpdateInstruction(cc_12, True) ]))
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_12, False) ]))
    st_12._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_13, [
        fac.UpdateInstruction(cc_13, True) ]))
    st_13._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
CTD_ANON_6._Automaton = _BuildAutomaton_7()




def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'KodFormularza')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 36, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'WariantFormularza')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 46, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'CelZlozenia')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 53, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DataWytworzeniaEParagon')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 54, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DomyslnyKodWaluty')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 59, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CTD_ANON_8._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'KodUrzedu')), pyxb.utils.utility.Location('/home/zpomianowski/eparagon-project/web2py/applications/eparagon/modules/eparagon_schema/EParagonSchema.xsd', 64, 3))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CTD_ANON_8._Automaton = _BuildAutomaton_8()

