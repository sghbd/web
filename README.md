# e-paragon
Frontend i backend do obsługi wydatków na bazie [e-paragonów](https://buchcom.pl/aktualnosci/1031-e-paragon-od-2018-r-jak-ma-funkcjonowac.html).
![Screen](screen.png)
Testowane na Ubuntu 16.04 LTS.

## Środowisko dev
### Depsy
Ze strony [Oracle'a](http://www.oracle.com/technetwork/database/features/instant-client/index-097480.html)
ściągnij klienta __oracle-instantclient12*.x86_64.rpm__ np. do _/tmp_.
Następnie stwórz wirtualne środowisko pythona i zainstaluj brakujące składniki:
```bash
# instalujemy depsy pod klienta Oracle; bazujemy na wersji 12.2!
sudo apt install alien dpkg-dev debhelper build-essential nodejs unzip git python-pyxb
# konwertujemy ściągniętą paczkę rmp na debianową
# instalujemy, ustawiamy odpowiednie zmienne środowiskowe
cd /tmp && chmod 755 oracle*
sudo alien -d oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
sudo dpkg -i oracle-instantclient12.2-basic_12.2.0.1.0-2_amd64.deb
echo "/usr/lib/oracle/12.2/client64/lib/" | \
sudo tee /etc/ld.so.conf.d/oracle.conf && sudo chmod o+r /etc/ld.so.conf.d/oracle.conf
echo "export ORACLE_HOME=/usr/lib/oracle/12.2/client64" | \
sudo tee /etc/profile.d/oracle.sh && sudo chmod o+r /etc/profile.d/oracle.sh

# potrzebny jest fix dla poprawnego działania npm
# linux unika nazwy 'node', bo utarło się, że ta nazwa jest zarezerwowana
# dla innych utilsów
ln -s /usr/bin/nodejs /usr/bin/node

# ściągamy framework
wget https://mdipierro.pythonanywhere.com/examples/static/2.14.6/web2py_src.zip
unzip web2py_src.zip
cd web2py/applications

# instalujemy apkę
git clone https://bitbucket.org/sghbd/web.git eparagon && cd eparagon
sudo nmp install -g webpack
npm install

# generujemy bundle - 'webpack -p' dla produkcji
webpack
virtualenv venv
source venv/bin/python
pip install -r requirements.txt
```

### Uruchomienie servera dev
Potrzebne jest wygenerowanie (bazujemy na __PyXB 1.2.4__) klas opisujących strukturę danych na podstawie plików _*.xsd_:
```bash
pyxbgen -m eparagon -u eparagon_schema/EParagonSchema.xsd
```
Zapewnia to dwukierunkową konwersję _XML <--> python_. Brak niestety prostego mechanizmu
na generowanie formularzy HTML. Dodatkowo generowanie klasy (domyślnie) mają ustawione absolutne ścieżki.

Odpal:
```bash
bash server.sh
```
Apka dostępna pod adresem: ``http://127.0.0.1:8000/eparagon``, ``http://10.8.0.14:8000/eparagon``

Panel główny admina znajdziesz pod adresem: ``http://127.0.0.1:8000/admin``.

Hasło: ``test``

## Środowisko produkcyjne
Najważniejsze składniki do instalacji/konfiguracji:
- __nginx__ - web server
- __uwsgi__ - middleware obsługujący interfejs _WSGI_ (_Web Server Gateway Interface_) przewidziany
jako uniwersalny interfejs dla aplikacji webowych Pythona (specyfikacja _PEP 333_)
- __oracle-instantclient12__ - biblioteki i narzędzie konieczne dla kompilacji sterownika cx_Oracle

```bash
# instalujemy podstawowe składniki potrzebne do dalszych kroków
sudo apt install -y libaio1 openvpn virtualenv nginx uwsgi uwsgi-plugin-python alien dpkg-dev debhelper build-essential unzip git python-pyxb

# instalujemy komponenty oracle - zaczynamy od skopiowania skonwertowanej wcześniej paczki
# deb z lokanego hosta na serwer produkcyjny
# musimy mieć klucz prywatny dający dostęp do serwera via SSH
scp -i ~/.ssh/eparagon.pem oracle-instantclient12.2-basic_12.2.0.1.0-2_amd64.deb \        
ubuntu@eparagon.org.pl:/tmp
# logoujemy się na serwer (użytkownik musi być sudoersem) i instalujemy z lokalnej paczki
sudo dpkg -i oracle-instantclient12.2-basic_12.2.0.1.0-2_amd64.deb
# musimy ponadto zadbać o odpowiednie zmienne środowiskowe
echo "/usr/lib/oracle/12.2/client64/lib/" | \
sudo tee /etc/ld.so.conf.d/oracle.conf && sudo chmod o+r /etc/ld.so.conf.d/oracle.conf
echo "export ORACLE_HOME=/usr/lib/oracle/12.2/client64" | \
sudo tee /etc/profile.d/oracle.sh && sudo chmod o+r /etc/profile.d/oracle.sh


# niekonieczne, ale ważne ustawienia polityki firewalla (zależy czy ufw jest aktywny)
sudo ufw allow 'Nginx HTTP'

# ze względów bezpecznieństwa serwera
# potrzebujemy dedykowanego użytkownika dla usług serwera webowego
sudo useradd nginx -m
sudo su nginx %% cd ~

# ściągamy i konfigurujemy framework
wget https://mdipierro.pythonanywhere.com/examples/static/2.14.6/web2py_src.zip && \
unzip web2py_src.zip && rm web2py_src.zip && cd web2py
cp handlers/wsgihandler.py .
cp examples/routes.parametric.example.py routes.py
# zmień domyślny routing
# default_application='welcome' -> default_application='eparagon'
# potrzebujemy hasła dla admina - konfiguracja dla konkretnego portu komunikacji SSL
python -c "from gluon.main import save_password; save_password(raw_input('wpisz hasło: '),443)"
# ściągamy aktulną wersję aplikacji (ostatni commit z gałęzi master)
# bb_user - użytkownik bitbucketa; uprawnieni: zpomianowski, Pivek1986, rafal_k0
cd applications && git clone https://<bb_user>@bitbucket.org/sghbd/web.git eparagon && cd ../..

# konfigurujemy środowisko wirtualne gwarantujące izolację modułów
# venv zabezpiecza przed konfliktami i przed potencjalną niekompatybilnością
# instalujemy potrzebne paczki i moduły pod kątem projektu
virtualenv venv && source venv/bin/activate
pip install -r web2py/applications/eparagon/requirements.txt

# musimy przepiąc się na sudoersa
sudo su ubuntu
sudo cp <app>/utils/web2py_nginx.conf /etc/nginx/sites-available/web2py
cd /etc/nginx/sites-enabled/ && sudo ln -s ../sites-available/web2py web2py && sudo rm default
sudo cp <app>/utils/web2py_emperor.xml /etc/uwsgi/apps-available
cd /etc/uwsgi/apps-enabled/ && sudo ln -s ../apps-available/web2py_emperor.xml web2py.xml

# generujemy odpowiednie klucze certyfikaty dla komunikacji SSL
sudo mkdir /etc/nginx/ssl
cd /etc/nginx/ssl
# generujemy parametry dla protokołu Diffiego-Hellmana
# (ustalenia wspólnego tajnego klucza przez publiczną komunikację
sudo openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048
# konwersje i klucze <days> - ważność klucza w dniach
sudo openssl genrsa -out server.key 2048
sudo openssl req -new -key server.key -out server.csr
sudo openssl x509 -req -days <days> -in server.csr -signkey server.key -out server.crt

# konfigurujemy openvpn - komunikacja ze zdalną bazą Oracle
cd /etc/openvpn
cp <XXX>/ca.crt . && cp <XXX>/user.crt && cp <XXX>/user.key && cp <XXX>/eparagon.openvpn
mv eparagon.ovpn eparagon.conf
# edytuj plik /etc/default/openvpn
# zamień #AUTOSTART="all" na AUTOSTART="all"
# niekoniecznie: sudo modprobe tun - jeśli nie ładowany jest ten moduł jądra
# openvpn lepiej zrestartować
sudo reboot
```

## Troubleshooting
TODO
- /usr/lib/libclntsh.so -> oracle/12.2/client64/lib/libclntsh.so.12.1 (symlink)
- libaio1 - brakujący lib
